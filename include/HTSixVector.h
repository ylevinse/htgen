// htgen/include/HTSixVector.h

class HTSixVector
  {
  public:
    HTSixVector(double x=0.,double xp=0., double y=0.,
                double yp=0., double ct=0., double dp=0.); // constructor
    virtual ~HTSixVector(){}; // empty destructor
    double x();
    double xp();
    double y();
    double yp();
    double ct();
    double dp();
    int set_x(double);
    int set_xp(double);
    int set_y(double);
    int set_yp(double);
    int set_ct(double);
    int set_dp(double);
    int fill(double x=0.,double xp=0., double y=0., double yp=0., double ct=0., double dp=0.);
    std::string PrintStr(); //hbu added for debug
  private:
    double v[6];
  };

