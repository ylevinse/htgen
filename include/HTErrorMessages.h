// htgen/include/HTErrorsMessage.h

enum HTMessageLevel { HT_ERROR_LEVEL=0 ,
  HT_WARNING_LEVEL=1,      // by default
  HT_INFO_LEVEL=2,         // for user information
  HT_DEBUG_LEVEL=0x4,      // general
  HT_DEBUG_PROCESS=0x8,    // related to process
  HT_DEBUG_INTERFACE=0x16
};
#ifdef HTDEBUG1
#define HT_DEBUG_ENABLE
#endif

class HTErrorMessages
{
  public:
    HTErrorMessages();
    HTErrorMessages(int);
    virtual ~HTErrorMessages(){}; // empty destructor
    int Message( int lvl, std::string arg,std::string msgtype,std::string filename,
                std::string functname, int linenr);
  private:
    static int printing_level;
    char envvar[20];
};

#define HT_ERROR(args)   do {                                   \
HTErrorMessages dte;                                  \
std::ostringstream arguments; arguments <<  args;\
dte.Message(HT_ERROR_LEVEL, arguments.str(),"*** ERROR in ",  __FILE__, __FUNCTION__ ,  __LINE__); \
} while (0);
#define HT_WARNING(args)   do {                                 \
HTErrorMessages dte;                                  \
std::ostringstream arguments; arguments <<  args;\
dte.Message(HT_WARNING_LEVEL, arguments.str(),"*** Warning in ",  __FILE__, __FUNCTION__ ,  __LINE__); \
} while (0);
#define HT_INFO(args)   do {                                    \
HTErrorMessages dte;                                  \
std::ostringstream arguments; arguments <<  args;\
dte.Message(HT_INFO_LEVEL, arguments.str(),"*** Info in ",  __FILE__, __FUNCTION__ ,  __LINE__); \
} while (0);
#define HT_DEBUG(args)   do {                                   \
HTErrorMessages dte;                                  \
std::ostringstream arguments; arguments <<  args;\
dte.Message(HT_DEBUG_LEVEL, arguments.str(),"*** DEBUG in ",  __FILE__, __FUNCTION__ ,  __LINE__); \
} while (0);


