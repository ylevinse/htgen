// Mat3x3.h
class Mat3x3 { public: // class for 3*3 matrices
    double m[3][3]; // the first index is the row (line) and the second the column number, see colletive/Math.tex with Matrix.tex
    // now the methods
    Mat3x3() {}; //Stroustrup2 p. 579; constructor, allow for empty init
    Mat3x3(const double& m00,const double& m01,const double& m02,
           const double& m10,const double& m11,const double& m12,
           const double& m20,const double& m21,const double& m22)
    {
      m[0][0]=m00;m[0][1]=m01;m[0][2]=m02;
      m[1][0]=m10;m[1][1]=m11;m[1][2]=m12;
      m[2][0]=m20;m[2][1]=m21;m[2][2]=m22;
    };
    friend Mat3x3 operator+ (const Mat3x3&,const Mat3x3&); // add two matrices
    friend Mat3x3 operator- (const Mat3x3&,const Mat3x3&); // diff of two matrices
    friend Mat3x3 operator* (const Mat3x3&,const Mat3x3&); // multiply two matrices
    friend Vec3 operator* (const Mat3x3&,const Vec3&); // multiply vector and matrix

    // Mat3x3 Transpose(void);  // return the transposed (without changing the original)
    const void PrintObj(std::ostream& FilOut); // print the matrix
};
double inline trace(const Mat3x3& M) {return M.m[0][0]+M.m[1][1]+M.m[2][2];}
double inline   det(const Mat3x3& M) // determinante, see Schaums lin.alg. p. 173
{return
    M.m[0][0]*(M.m[1][1]*M.m[2][2]-M.m[1][2]*M.m[2][1]) -
    M.m[0][1]*(M.m[1][0]*M.m[2][2]-M.m[1][2]*M.m[2][0]) +
    M.m[0][2]*(M.m[1][0]*M.m[2][1]-M.m[1][1]*M.m[2][0]);}
Mat3x3 Transpose(const Mat3x3&); // return the transposed Matrix (without changing the original)

