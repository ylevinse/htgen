//  htgen/include/HTBaseInterface.h

// define a ProcessMap
// allows to address process using the name, currently mott or brem and the mean free path
typedef std::map< std::string, double > ProcessMap;

class HTBaseInterface
{
  public:
    HTBaseInterface(); // {};  // empty constructor
    virtual ~HTBaseInterface(){};  // empty destructor
    int addProcess(std::string);// add a process [mott, brem]
    int allProcess(); // all process are taken into account
    double getMeanFreePath(int flg=0); // return mean free path: flg=1, recalculate processes meanfreepaths
    std::string getProcessType(); //  get the type of process
    int setConfiguration(HTConfiguration);//  set configuration
    int setPressure(double); //  set pressure in Pa
    int setTemperature(double); //  set temperature in K
    int setZ(double); //  set Z mean
    int setThetamin(double); //  set thetamin for mott
    int setKmin(double); //  set kmin for brem
    int setEbeam(double);//  set beam energy
    int setNpart(double);//  set particles/bunch
    HTConfiguration getConfiguration();//  return configuration
    int getNbEvents(double lambda, double length, int fstep=5);//  return  #events: fstep -> accuracy (def.=5)
  protected:
    std::string interface; //  interface name
    ProcessMap mprocess; //  map <process, cross section>
    //  beam parameters
    HTConfiguration param;
    HTMottProcess *mott;
    HTBremProcess *brem;
  private:
    virtual int init(); // (empty) init function
};
