//  htgen/include/HTBaseProcess.h

double RanHtgen(void); // random generator supplied with htgen

class HTBaseProcess
{
  public:
    HTBaseProcess(); // constructor
    HTBaseProcess(std::string);   // constructor with parameter file name
    HTBaseProcess(HTConfiguration);
    virtual ~HTBaseProcess(){}; // empty destructor
    virtual int generateDistribution(std::string, int);
    virtual double getCrossSection();
    HTTextParser* getTextParser();
    HTConfiguration getConfiguration();
    std::string process;
    int setConfiguration(HTConfiguration);
    int setConfiguration(HTTextParser*);
    int setEbeam(double);
    int setTemperature(double);

  protected:
    HTConfiguration conf;
    HTTextParser* parser;
    double sigma;
  private:
    virtual int init();
    std::string parsername;
};


