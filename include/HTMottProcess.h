//  htgen/include/HTMottProcess.h

class HTMottProcess:
  public HTBaseProcess
  {
  public:
    HTMottProcess(std::string fname);
    HTMottProcess(HTConfiguration config);
    virtual ~HTMottProcess(){}; // empty destructor
    double getCrossSection();
    double getMeanFreePath(HTConfiguration);
    int setThetamin(double);
    void kickParticle(double& xp, double& yp);
    void generateEvent();
	void getGeneratedValues(double& e,double& theta,double& phi);
  private:
	double sinthetae,phi;
    double thetamin_factor; // 1./(1.-cos(thetamin));
  };
