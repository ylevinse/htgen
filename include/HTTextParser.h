//  htgen/include/HTTextParser.h

class HTTextParser
{
  public:
    HTTextParser();
    HTTextParser(const char* name);
    virtual ~HTTextParser(){};
    int  parseFile(const char* name);
    bool isgood;
    std::string getFileName();
    int print();
    HTConfiguration getConfiguration();
    double ebeam;
    double pressure;
    double Z;
    double thetamin;
    double L;
    double npart;
    double kmin;
    double T;
  private:
    std::ifstream fin;
    std::string fname;
    int init();
    HTConfiguration conf;
    int fillConfiguration();
};

