// Vec3.h
class Spherical;class Vec3;class Vec4;class Mat3x3; // class forward declarations

class Vec3 { // use no virtual to allow initialization like Vec3 vec={0,0,0};
public:
    double r[3]; // x,y,z
    // now the methods
    Vec3() {}; //Stroustrup2 p. 579; constructor, allow for empty init
    Vec3 (const double& x,const double& y,const double& z) {r[0]=x;r[1]=y;r[2]=z;} // constructor
/*
    Vec3 (const Spherical& v) // construct from Spherical
    { r[0]=v.r*sin(v.theta)*cos(v.phi); // x
      r[1]=v.r*sin(v.theta)*sin(v.phi); // y
      r[2]=v.r*cos(v.theta);            // z
    }
 */
    Vec3 (const Vec4& q); // {r[0]=q.p[1];r[1]=q.p[2];r[2]=q.p[3];} // init from Vec4
    ~Vec3 () { } // (empty) destructor

    friend Vec3 operator+ (const Vec3&,const Vec3&);    //Stroustrup3 282, add two three vectors
    friend Vec3 operator- (const Vec3&);    //Stroustrup3 282, change sign of vector part
    friend Vec3 operator- (const Vec3&,const Vec3&);    // difference of 2 three vector
    friend Vec3 operator^ (const Vec3&,const Vec3&);    //Stroustrup3 282, three vector cross product
    friend Vec3 operator* (double, const Vec3&);    //Stroustrup3 282, factor * three vector
    friend Vec3 operator/ (const Vec3&,double); // three vector devided by double
    friend double   operator* (const Vec3& v1,const Vec3& v2)   //Stroustrup3 282, three vector scalar product
    { return v1.r[0]*v2.r[0]+v1.r[1]*v2.r[1]+v1.r[2]*v2.r[2];}

    // possible to define abs2 and abs as inline inspector functions, more natural outside, see below
    // double inline abs2(void) const {return      r[0]*r[0]+r[1]*r[1]+r[2]*r[2] ;} // 3 momentum squared, const because changes nothing in class (const function or inspector) Stroustrup3 229
    // double inline abs (void) const {return sqrt(r[0]*r[0]+r[1]*r[1]+r[2]*r[2]);} // abs 3 momentum, const because changes nothing in class (const function or inspector) Stroustrup3 229
    void ParallelPerp(const Vec3& b,Vec3& rParallel,Vec3& rPerp);
//    Spherical ToSpherical(void);    // from Vec3 to Spherical
    Mat3x3  GetRotToZMat(void); // returns the matrix which rotates the Vec3 to +z direction
    const void PrintObj(std::ostream& FilOut);
};

// rather define outside class, makes calling more natural  abs(vec) instead of vec.abs()
inline double abs2(const Vec3& v) {return      v.r[0]*v.r[0]+v.r[1]*v.r[1]+v.r[2]*v.r[2] ;} // 3 momentum squared
inline double abs (const Vec3& v) {return sqrt(v.r[0]*v.r[0]+v.r[1]*v.r[1]+v.r[2]*v.r[2]);} // abs of three momentum

