//  htgen/include/HTPlacetInterface.h
#include <fstream>

// forward declaration
class BEAM;
struct PARTICLE; // structure double energy,wgt,y,yp,z; defined in  placet-development/src/particle.h

class HTPlacetInterface :
  public HTBaseInterface
  {
  public:
    HTPlacetInterface();    // constructor
    virtual ~HTPlacetInterface(){}; // emtpy destructor
    void makeEvent(BEAM*,std::ostringstream&);
    int makeHalo(BEAM*, double,std::ostringstream&);
    int makeMsc(PARTICLE&, double, double);
    BEAM* makeDummyBeam(BEAM*);
    int makeNewBeam(BEAM*, PARTICLE& , int);
    PARTICLE getRandomParticle(BEAM*, int&);
    int dumpBeam(BEAM*); // print info about beam, used in TestPlacet.cxx
  private:
    void print_htgen_info(BEAM*); //hbu print simple info about the beam
  };
