//  htgen/include/HTBremProcess.h

class HTBremProcess:
  public HTBaseProcess
  {

  public:
    HTBremProcess(std::string fname);
    HTBremProcess(HTConfiguration config);
    virtual ~HTBremProcess(){}; // empty destructor
    double getCrossSection();
    double getMeanFreePath(HTConfiguration);
    void kickParticle(double& xp,double& yp);
    void generateEvent();
    int setKmin(double);
	void getGeneratedValues_Electron(double& e,double& theta,double& phi);
	void getGeneratedValues_Photon(double& e,double& theta,double& phi);
  private:
	double xk,sinthetae,thetap,phi;
    double kmin;
  };
