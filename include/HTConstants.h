// htgen/include/HTConstants.h

static const double c_k     = 1.3806505E-23;    // Boltzmann constant in Joule / Kelvin
static const double c_c     = 299792458;        // speed of light in m/s
static const double c_alpha = 1./137.03599911;  //  fine structure constant
static const double c_re    = 2.817940325E-15;  //  classical electron radius = Echarg / (4 pi eps0) 1.e9 Mele)
static const double c_mele  = .51099892E-03;    //  electron mass in GeV /c^2
static const double c_pi    = 4.*atan(1.);
static const double c_AlfaQEDHbarc=1.439964398e-18; // AlfaQED*Hbarc=Re Mele c**2=1.439964398e-18 GeV meter

//
// units
//

// lengths
static const double u_m = 1.;
static const double u_mm = 1e-3 * u_m;
static const double u_um = 1e-6 * u_m;

static const double u_Barn=1.e28; // 1 squaremeter in barn
