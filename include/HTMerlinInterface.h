/**
    \file HTMerlinInterface.h
    \brief    Interface classes for tracking in merlin
    \author L. Neukermans
    \version 0.1
    \date 11/2005
*/
#ifndef _HTMerlinInterface_H_
#define _HTMerlinInterface_H_

#include<list>

// htgen headers
#include "HTBaseInterface.h"

// merlin headers
#include "AcceleratorModel/AcceleratorModel.h"
#include "BeamDynamics/ParticleTracking/ParticleBunch.h"
#include "BeamDynamics/ParticleTracking/ParticleBunchProcess.h"
#include "BeamModel/BeamData.h"

#define COLL_AT_ENTRANCE 1
#define COLL_AT_CENTER 2
#define COLL_AT_EXIT 4

class HTMerlinInterface: public HTBaseInterface {
 public:
  HTMerlinInterface();
  HTMerlinInterface(const string&);
  ~HTMerlinInterface();
  /** \brief construct accelerator model from mad output and default beam */
  std::pair<AcceleratorModel*,BeamData*> constructModel(const string&);
  /** \brief construct the initial beam */
  ParticleTracking::ParticleBunch* constructBunch(int);
  AcceleratorModel* getAcceleratorModel();
  int makeHalo(double);
 private:
  int init();
  int makeEvent();
  PSvector getRandomParticle(int &);
  AcceleratorModel* theacc;
  BeamData* thebeam;
  ParticleTracking::ParticleBunch* thebunch;
};

class HTGenParticleProcess: public ParticleTracking::ParticleBunchProcess {
 public:
    typedef map<string,int> IDTBL;
  HTGenParticleProcess (int priority, int mode, std::ostream* osp = 0);
  ~HTGenParticleProcess ();
  virtual void InitialiseProcess (Bunch& bunch);
  virtual void SetCurrentComponent (AcceleratorComponent& component);
  virtual void DoProcess(double);
  virtual double GetMaxAllowedStepSize () const;
  int setHTInterface(HTMerlinInterface*);
  void CreateParticleLossFiles (bool flg, std::string fprefix = "");
  void SetLossThreshold (double);
  void IndexParticles (bool);
  void IndexParticles (std::list<size_t>& anIndex);
 protected:
  int cmode;
  std::ostream* os;
  bool createLossFiles;
  string file_prefix;
  double lossThreshold;
    IDTBL idtbl;
 private:
  int DoHalo();
  int DoCollimation();
  int SetNextS();
  bool DoScatter (ParticleTracking::Particle&);
  void DoOutput (const PSvectorArray&, const list<size_t>&);
  HTMerlinInterface* theiface;
  double s;
  double next_s;
  double s_total;
  size_t nstart;
  size_t nstart0;
    bool at_entr;
    bool at_cent;
    bool at_exit;
  size_t nlost;
  size_t nhalo;
  int idcomp;
  std::list< size_t >* pindex;
  bool scatter;
  bool is_spoiler;
  double Xr; // radiation length
  double len; // physical length
};
#endif
