//  htgen/include/HTMscProcess.h

class HTMscProcess
  {
  public:
    //HTMscProcess(); // {}; // constructor
    virtual ~HTMscProcess(){}; // empty destructor
    virtual void ScatWithGauss(double& g1,double& g2);
    virtual std::vector<HTSixVector> getSixVector(std::string) { return vsv;};
    virtual HTSixVector kickParticle(HTSixVector );
    virtual void setEb(double ebeam){ this->ebeam=ebeam; };
    virtual void setX0(double X0){ this->X0=X0; };
    virtual void setPath(double length){ this->length=length; };
    virtual double getLoss(void);
  private:
    std::vector<HTSixVector> vsv;
    double ebeam;
    double X0;
    double length;
    std::vector<double> vlossprob;
  };

