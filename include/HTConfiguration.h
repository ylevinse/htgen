//  htgen/include/HTConfiguration.h

class HTConfiguration
{
  public:
    HTConfiguration(); // constructor
    virtual ~HTConfiguration(){;} // empty destructor
    // specific to accelerator/beam
    double ebeam; // beam energy [GeV]*/
    double L; // accelerator length [m]
    double T; // temperature [K]
    double npart; // particles/bunch
    double pressure; // gas pressure [Pa]
    double Zmean; // Mean Z gas
    // specific to process
    double thetamin; // Minimum integration angle for Mott [rad]
    double kmin; // Minimum photon energy for Brem. [fraction electron E]
    // info
    int dump(); // print current values
};
