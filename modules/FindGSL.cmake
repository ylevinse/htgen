# Need to find some basic GSL stuff
# We don't actually use GSL, but during
# compilation, PLACET headers need this
#
# If you have GSL in a non-standard location,
# set GSL_PREFIX such that $GSL_PREFIX/bin/gsl-config
# exists
#
# This module defines:
# GSL_FOUND           True if GSL is found
# GSL_PREFIX          Path to GSL installation
# GSL_INCLUDE_DIRS    GSL include directories

if(NOT DEFINED GSL_PREFIX)
  find_program(GSL_CONFIG gsl-config)
  mark_as_advanced(GSL_CONFIG)
  execute_process(COMMAND ${GSL_CONFIG} --prefix
    OUTPUT_VARIABLE GSL_PREFIX
    OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

set(GSL_INCLUDE_DIRS "${GSL_PREFIX}/include")
if(IS_DIRECTORY "${GSL_INCLUDE_DIRS}/gsl")
  set(GSL_FOUND True)
else()
  set(GSL_FOUND False)
endif()

if(GSL_FOUND)
  if(NOT GSL_FIND_QUIETLY)
    message( STATUS "Found GSL: ${GSL_PREFIX}" )
  endif(NOT GSL_FIND_QUIETLY)
else(GSL_FOUND)
  if(GSL_FIND_REQUIRED)
    message(FATAL_ERROR "FindGSL: Could not find GSL")
  endif(GSL_FIND_REQUIRED)
endif(GSL_FOUND)
