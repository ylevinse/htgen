/*
Program     : TestMerlin
Author      : L. Neukermans
Date        : 31/01/06
Description : Test of Merlin interface
 */

#include "BeamDynamics/ParticleTracking/ParticleTracker.h"
#include "HTMerlinInterface.h"

// ------------- config
string lattice = "/afs/cern.ch/user/n/neukerma/local/eurotev/HTGEN/config/linbds.lattice.txt";
int npart = 10000;
// ------------- end config

using namespace ParticleTracking;

int main(int argc, char **argv)
{
  cout<<"[TestMerlin] enter ..."<<endl;

  HTMerlinInterface *merlin = new HTMerlinInterface(lattice);
  merlin->addProcess("mott");
  merlin->addProcess("brem");
  merlin->setThetamin(1.e-7);
  merlin->setPressure(1.e-6);
  merlin->setKmin(0.01);
  merlin->setEbeam(250.);
  merlin->setZ(7);
  merlin->setTemperature(300);
  merlin->setNpart(2.e10);

  ParticleTracker tracker(merlin->getAcceleratorModel()->GetBeamline(),
              merlin->constructBunch(npart),true);

  ofstream lossSummaryOS("out/loss_summary.dat");
  HTGenParticleProcess* proc =
    new HTGenParticleProcess(2,COLL_AT_EXIT,&lossSummaryOS);
  proc->setHTInterface(merlin);
  // Create loss particle files in data/
  proc->CreateParticleLossFiles(true,"out/");
  // Abort tracking when 100% particles are lost.
  proc->SetLossThreshold(100.0);
  // Index the loss particle files
  proc->IndexParticles(true);
  tracker.AddProcess(proc);

   bool dump_particles = true;
   try {
     tracker.Run();
     if(dump_particles) {
       ofstream os("out/particles.out.dat");
       tracker.GetTrackedBunch().Output(os);
     }
   }
   catch(MerlinException& err) {
     cout<<err.Msg()<<endl;
   }

   return 0;
}


