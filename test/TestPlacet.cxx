// htgen/src/TestPlacet.cxx

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
//#include <vector>
#include <map>

#include "HTSixVector.h"
#include "HTErrorMessages.h"

#include "HTConstants.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"
#include "HTBaseInterface.h"
#include "HTPlacetInterface.h"

using namespace std;

int main(int argc, char **argv)
{
  cout<<"[TestPlacet] enter ..."<<time(0)<<endl;
  srand(time(0));
  BEAM* beam;
  // Define a beamline parameter structure
  HTTextParser parser("TESLA.par");
  parser.print();
  // Define the interface
  HTPlacetInterface placet;
  // Parse the parameters
  placet.setConfiguration(parser.getConfiguration());
  // Choose particles processes
  placet.addProcess("mott");
  placet.addProcess("brem");
  // get meanfreepath in meters
  placet.getMeanFreePath();
  // Here make dummy beam for tests purpose
  beam = placet.makeDummyBeam(beam);
  placet.dumpBeam(beam);
  // Make ONE event
  placet.makeEvent(beam,(ostringstream&)cout);

  return 0;
}
