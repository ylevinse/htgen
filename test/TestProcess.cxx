// htgen/src/TestProcess.cxx
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

// root
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>

#include "HTSixVector.h"
#include "HTErrorMessages.h"

#include "HTConstants.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"

using namespace std;

// ---------------------------------  default values
static string ptype = "mott";   // default process type: mott, alternative is brem
static int nevt= (int)  1.e6;	// events to be generated
static double thetamax=1.e-6;  // theta max for plotting
// ------------------------------------------------

int main(int argc, const char* argv[])
{
  bool NormToCrossSection=true;
  HT_DEBUG("TestProcess starts");
  // Define a beamline parameter structure
  double sigma=0, lambda=0, rho=0, proba=0, nscat=0, tau=0;

  string HTGEN_DIR=getenv("HTGEN_DIR");
  string InputFileName=HTGEN_DIR+"/config/ILC-LIN.par"; // default input filename
  HT_DEBUG(" default InputFileName=" << InputFileName);
  if(argc>1)
  {
    InputFileName=argv[1];
    HT_DEBUG("use InputFileName=" << InputFileName << " given in command line ");
  }
  if(argc>2) ptype=argv[2]; // mott or brem
  if(argc>1) HT_DEBUG("use ptype=" << ptype << " defined from command line ");

  HTConfiguration conf;
  string RootFileName=ptype+".root";
  TFile RootFile(RootFileName.c_str(),"recreate");
  cout << "Histograms will be written to " << RootFileName << '\n';

  if(ptype=="mott")
  {
    HTMottProcess mott(InputFileName);
    sigma = mott.getCrossSection();

	int nbin=1000;
    TH1F h1("electron_energy","electron : energy",nbin,0.,1.1);
    TH1F h2("electron_theta", "electron : theta ",nbin,0.,thetamax);
    TH1F h3("electron_phi"  , "electron : phi   ",nbin,-c_pi,c_pi);

	for(int i=0;i<nevt;i++)
	{
	  mott.generateEvent();
	  double el_e,el_theta,el_phi;
	  mott.getGeneratedValues(el_e,el_theta,el_phi);
	  h1.Fill(el_e);
	  h2.Fill(el_theta);
	  h3.Fill(el_phi);
	  if(i<10) cout << " el_e=" << setw(11) << el_e << " el_theta=" << setw(11) << el_theta << " el_phi=" << setw(11) << el_phi << '\n';
	}
	cout << "mott after filling with el_e     h1.GetEntries()=" << h1.GetEntries() << " mean=" <<  h1.GetMean() << '\n';
	cout << "mott after filling with el_theta h2.GetEntries()=" << h2.GetEntries() << " mean=" <<  h2.GetMean() << '\n';
	cout << "mott after filling with el_phi   h3.GetEntries()=" << h3.GetEntries() << " mean=" <<  h3.GetMean() << '\n';
	if(NormToCrossSection)
	{
	  h1.Scale(sigma*u_Barn/h1.GetEntries());
	  h2.Scale(sigma*u_Barn/h2.GetEntries());
	  h3.Scale(sigma*u_Barn/h3.GetEntries());
	}
    h1.Write();
    h2.Write();
    h3.Write();

    conf = mott.getConfiguration();
    lambda = mott.getMeanFreePath(conf);

    rho = conf.pressure/(c_k*conf.T)*2; // 2 comes from N nucleus/molecul
    proba = rho*sigma;
    nscat = proba*conf.L*conf.npart;
    tau = 1./proba/c_c;

  }
  else if(ptype=="brem")
  {
    HTBremProcess brem(InputFileName);
    sigma = brem.getCrossSection();
    int nbin=1000;
    TH1F h1_e("electron_energy","electron : energy",nbin,0.,1.);
    TH1F h2_e("electron_theta", "electron : theta ",nbin,0.,thetamax);
    TH1F h3_e("electron_phi"  , "electron : phi   ",nbin,-c_pi,c_pi);
    TH1F h1_p("photon_energy","photon : energy",nbin,0.,1.);
    TH1F h2_p("photon_theta", "photon : theta ",nbin,0.,10.*thetamax);
    TH1F h3_p("photon_phi"  , "photon : phi   ",nbin,-c_pi,c_pi);
	for(int i=0;i<nevt;i++)
	{
	  brem.generateEvent();
	  double el_e,el_theta,el_phi;
	  double ga_e,ga_theta,ga_phi;
	  brem.getGeneratedValues_Electron(el_e,el_theta,el_phi);
	  brem.getGeneratedValues_Photon(ga_e,ga_theta,ga_phi);
	  h1_e.Fill(el_e);
	  h2_e.Fill(el_theta);
	  h3_e.Fill(el_phi);
	  h1_p.Fill(ga_e);
	  h2_p.Fill(ga_theta);
	  h3_p.Fill(ga_phi);
	  if(i<10) cout << " el_e=" << setw(11) << el_e << " el_theta=" << setw(11) << el_theta << " el_phi=" << setw(11) << el_phi
                    << " ga_e=" << setw(11) << ga_e << " ga_theta=" << setw(11) << ga_theta << " ga_phi=" << setw(11) << ga_phi << '\n';
	}
	cout << "beam after filling with el_e     h1_e.GetEntries()=" << h1_e.GetEntries() << " mean=" <<  h1_e.GetMean() << '\n';
	cout << "brem after filling with el_theta h2_e.GetEntries()=" << h2_e.GetEntries() << " mean=" <<  h2_e.GetMean() << '\n';
	cout << "brem after filling with el_phi   h3_e.GetEntries()=" << h3_e.GetEntries() << " mean=" <<  h3_e.GetMean() << '\n';
	cout << "beam after filling with ga_p     h1_p.GetEntries()=" << h1_p.GetEntries() << " mean=" <<  h1_p.GetMean() << '\n';
	cout << "brem after filling with ga_theta h2_p.GetEntries()=" << h2_p.GetEntries() << " mean=" <<  h2_p.GetMean() << '\n';
	cout << "brem after filling with ga_phi   h3_p.GetEntries()=" << h3_p.GetEntries() << " mean=" <<  h3_p.GetMean() << '\n';	
	if(NormToCrossSection)
	{
      h1_e.Scale(sigma*u_Barn/h1_e.GetEntries());
      h2_e.Scale(sigma*u_Barn/h2_e.GetEntries());
      h3_e.Scale(sigma*u_Barn/h3_e.GetEntries());
      h1_p.Scale(sigma*u_Barn/h1_p.GetEntries());
      h2_p.Scale(sigma*u_Barn/h2_p.GetEntries());
      h3_p.Scale(sigma*u_Barn/h3_p.GetEntries());
	}
    h1_e.Write();
    h2_e.Write();
    h3_e.Write();
    h1_p.Write();
    h2_p.Write();
    h3_p.Write();

    conf = brem.getConfiguration();
    lambda = brem.getMeanFreePath(conf);

    rho = conf.pressure/(c_k*conf.T);
    proba = rho*sigma;
    nscat = proba*conf.L*conf.npart;
    tau = 1./proba/c_c;
  }
  else
  {
    cout << "[TestProcess] ERROR : process " << ptype << " does not exist" << endl;
    return 1;
  }

  // results
  printf("Cross section       : %g barn \n", sigma*u_Barn);
  printf("Mean free path      : %e  m \n", lambda);
  printf("Particle density    : %e /m3 \n", rho);
  printf("Scattering prob.    : %e /m \n", proba);
  printf("Scatter./bunch      : %e  \n", nscat);
  printf("scattering fraction : %e \n", nscat/conf.npart);

  RootFile.Close();
  cout << "TestProcess done. Histograms written to " << RootFileName << '\n';

  return 0;
}


