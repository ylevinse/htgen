/* htgen/src/TestMultipleScatt.cxx

  debug on off :
export HTGEN_DEBUG=16
unset  HTGEN_DEBUG
  make :
source ~/c/htgen/env.csh ; cd $HTGEN_DIR ; make libhtgen ; cd $HTGEN_DIR/test ; make TestMultipleScatt
  run :
cd /tmp/$LOGNAME/ ; rm -f msc.root ; $HTGEN_DIR/test/bin/TestMultipleScatt 250. 8.6e-3 0.35 100000

*/

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

// root
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>

#include "HTSixVector.h"
#include "HTErrorMessages.h"
#include "HTMscProcess.h"

using namespace std;

int main(int argc, char **argv)
{
  // default values
  double ebeam=250.;   // ILC
  double l=8.6e-3;
  double X0=0.35; // Beryllium X0 ~ 35.cm
  int nevt= (int) 5e5;
  if(argc>1)
  {
    istringstream istr(argv[1]);
    istr >> ebeam;
  }
  if(argc>2)
  {
    istringstream istr(argv[2]);
    istr >> l;
  }
  if(argc>3)
  {
    istringstream istr(argv[3]);
    istr >> X0;
  }
  if(argc>4)
  {
    istringstream istr(argv[4]);
    istr >> nevt;
  }
  cout << "TestMultipleScatt"
  << " ebeam=" << ebeam << " GeV  "
  << " l=" << l << " m  "
  << " X0=" << X0 << " m  "
  << " nevt=" << nevt
  << '\n';

  const char* RootFileName="msc.root";
  TFile RootFile(RootFileName,"recreate");
  cout << "Histograms will be written to " << RootFileName << '\n';

  int nbin=1000;
  // double xmax =1.e-4,xpmax=1.e-2,elmax=1.e-5; // normal test
  double xmax =1.e-4,xpmax=1.e-2,elmax=1.e-2; // normal test
  // double xmax =1.e-3,xpmax=1.e-3,elmax=1.e-3; // scraper test
  TH1F h1("msc_x" ,"msc: x" ,nbin,-xmax,  xmax);
  TH1F h2("msc_xp","msc: xp",nbin,-xpmax,xpmax);
  TH1F h3("msc_y", "msc: y" ,nbin,-xmax,  xmax);
  TH1F h4("msc_yp","msc: yp",nbin,-xpmax,xpmax);
  TH1F h5("msc_ct","msc: ct",nbin,-1.e-5,1.e-5);
  TH1F h6("msc_dp","msc: dp",nbin,-elmax,elmax);

  // HTMscProcess msc(InputFileName); // this reads the InputFileName
  HTMscProcess msc;
  msc.setPath(l);
  msc.setX0(X0);     // Beryllium X0 ~ 35.cm
  msc.setEb(ebeam);
  // msc.getLoss(0); //init
  HTSixVector vin;
  for(int i=0;i<nevt;i++)
  {
    if(i%10000==0) cout << " Generated " << i << '\n';
    HTSixVector vout=msc.kickParticle(vin);
    h1.Fill(vout.x());
    h2.Fill(vout.xp());
    h3.Fill(vout.y());
    h4.Fill(vout.yp());
    h5.Fill(vout.ct());
    h6.Fill(vout.dp());
	if(i<10) cout << " vin=" <<  vin.PrintStr() << '\n';
	if(i<10) cout << "vout=" << vout.PrintStr() << '\n';
  }
  h1.Write();
  h2.Write();
  h3.Write();
  h4.Write();
  h5.Write();
  h6.Write();

  RootFile.Close();
  cout << "TestMultipleScatt done. Histograms written to " << RootFileName << '\n';

  return 0;
}

