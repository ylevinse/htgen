// htgen/src/TestProcessLinac.cxx

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>

#include "HTErrorMessages.h"
#include "HTConstants.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"

using namespace std;

// ---------------------------------  default values
static double emittance  = 3.e-8; // normalised emittance in m
static double E1 = 5; // entrance energy in GeV
static double E2 = 250; // output energy in GeV
static double beta = 100; // mean beta function in m
static double Length = 11000; // linac in m
static int maxstep = 10000; // number of step for computation
static double Temperature = 2; // temperature in K
static double Nsigma = 30.; // sigma from thetamin

int main(int argc, const char* argv[])
{
  HT_DEBUG("TestPorcessLinac starts");
  // Define a beamline parameter structure
  double sigma=0, rho=0, proba=0, nscat=0; // lambda=0

  string HTGEN_DIR=getenv("HTGEN_DIR");
  string InputFileName=HTGEN_DIR+"/config/ILC-LIN.par"; // default input filename
  HT_DEBUG("default InputFileName=" << InputFileName);
  if(argc>1) InputFileName=argv[1];
  if(argc>1)  HT_DEBUG("use InputFileName=" << InputFileName << " given in command line ");

  HTMottProcess mott(InputFileName);

  HTConfiguration conf = mott.getConfiguration();

  double ds = Length/(double) maxstep;
  double dE = (E2-E1)/(double) maxstep;
  double dgamma = dE/c_mele;
  double  gammacurrent = E1/c_mele;
  double Ecurrent = E1;
  mott.setTemperature(Temperature);
  HT_DEBUG("maxstep=" << maxstep);
  for(int istep = 0; istep < maxstep; istep++)
  {
    mott.setEbeam(Ecurrent);
    mott.setThetamin(Nsigma*sqrt(emittance/gammacurrent/beta));
    sigma = mott.getCrossSection();

    rho = conf.pressure/(c_k*Temperature);
    proba = rho*sigma;
    nscat += proba*ds*conf.npart;

    Ecurrent += dE;
    gammacurrent += dgamma;
  }

  double xs = (4.*c_pi*4*2.817e-15*2.817e-15*100*c_mele)/(30*30*3e-8*10);
  cout << " xs=" << xs << endl;
  double thetamin = Nsigma*sqrt(emittance/10*c_mele/beta);
  double Const = c_pi * (1./137*6.582e-22*1.e-3*c_c)* (1./137*6.582e-22*1.e-3*c_c);
  double xs2 = Const * 2*2/10/10/(1-cos(thetamin));
  cout << " Const=" << Const << "   xs2=" << xs2 << endl;
  printf("Cross section       : %.10g barn \n", sigma*u_Barn);
  printf("Particle density    : %e /m3 \n", rho);
  printf("Scattering prob.    : %e /m \n", proba);
  printf("Scatter./bunch      : %e  \n", nscat);
  printf("scattering fraction : %e \n", nscat/conf.npart);

  return 0;
}

