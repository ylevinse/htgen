#!/bin/bash
#
# setup environment for htgen compilation


echo "------------------";
echo "This script will setup environment variables for HTGen compilation"
echo "------------------";

echo "------------------ Setting Root environnement"
echo "Needed only for the histogrammer library"
echo "------------------"
echo "What is the ROOT installation directory"?
echo "[default] ${ROOTSYS} :"  ;
read answer;
_root_base_dir=$answer;
if [ -z $answer ]; then
    _root_base_dir=$ROOTSYS
fi
echo "ROOTSYS is set to: $_root_base_dir "
echo "------------------"
echo ""

echo "------------------ Setting Placet environnement"
echo "Needed for placet interface library"
echo "------------------"
echo "What is the Placet installation directory"?
echo "[default] ${PLACET_DIR} :"  ;
read answer;
_placet_base_dir=$answer;
if [ -z $answer ]; then
    _placet_base_dir=$PLACET_DIR
fi
echo "PLACET_DIR is set to: $_placet_base_dir "
echo "------------------"
echo ""

echo "------------------ Setting MERLIN environnement"
echo "Needed only for the merlin interface library"
echo "------------------"
echo "What is the Merlin source directory"?
echo "[default] ${MERLIN_SRC} :"  ;
read answer;
_merlin_base_dir=$answer;
if [ -z $answer ]; then
    _merlin_base_dir=$MERLIN_SRC
fi
echo "MERLIN_SRC is set to: $_merlin_base_dir "
echo "------------------"
echo ""

export HTGEN_DIR=$PWD
export ROOTSYS=$_root_base_dir
export PLACET_DIR=$_placet_base_dir
export MERLINSRC=$_merlin_base_dir

cat > env.sh <<EOF
export HTGEN_DIR=$PWD
export ROOTSYS=$_root_base_dir
export PLACET_DIR=$_placet_base_dir
export MERLINSRC=$_merlin_base_dir

# export PATH=$PATH:$ROOTSYS/bin
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$HTGEN_DIR/lib

echo "environment variables are set"
EOF

cat > env.csh <<EOF
setenv HTGEN_DIR $PWD
setenv ROOTSYS $_root_base_dir
setenv PLACET_DIR $_placet_base_dir
setenv MERLINSRC $_merlin_base_dir

# setenv PATH $PATH:$ROOTSYS/bin
# setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH:$ROOTSYS/lib:$HTGEN_DIR/lib

echo "environment variables are set"
EOF

# check if gsl-config is present
var=`which gsl-config`
if [ ! -e "$var" ]; then
echo ""
echo "*** Warning *** gsl-config is not found on your system. Ok for htgen alone."
echo "Needed for placet and the interface shared library libhtplacet"
echo "Please locate this lib and add the location to your path"
echo "To test if gsl is in the current path type :   which gsl-config"
exit 0
fi

echo "env.sh and env.csh have been created"
echo "type if you are in bash [csh] "
echo "$ source env.sh [env.csh] "
echo "$ make"
