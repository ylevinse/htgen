# --------------------------------------------------------------------------
# Makefile for library compilation,  H. Burkhardt
# 1st version 31/10/2005 by L. Neukermans
# --------------------------------------------------------------------------
###---------------------------------- general paths

SRC 	 = ./src/
OBJ	 = ./obj/
INC 	 = ./include/
LIB	 = $(PWD)/lib/

###---------------------------------- includes 
INCLPATH = 	   -I $(INC)

# ifneq ($(MERLINSRC),) 
#	INCLPATH += -I $(MERLINSRC)
#	MERLINLIBS   :=/local/neukerma/eurotev/merlin-unix/lib
# endif

ifneq ($(PLACET_DIR),) 
	INCLPATH += -I $(PLACET_DIR)/include -I $(PLACET_DIR)/include-utils -I $(PLACET_DIR)/src
	INCLPATH += $(shell gsl-config --cflags)
	GSLLIBS      := $(shell gsl-config --libs)
endif
ifneq ($(ROOTSYS),) 
	INCLPATH += -I $(shell $(ROOTSYS)/bin/root-config --incdir)
	ROOTLIBS     := $(shell $(ROOTSYS)/bin/root-config --libs) -lEG
endif

# if INSTALL_PREFIX is not defined, set it by default to /usr/local/
ifeq ($(INSTALL_PREFIX),)
	INSTALL_PREFIX := /usr/local
endif

ObjSuf        = o
SrcSuf        = cxx
IncSuf        = h
ExeSuf        =
DllSuf        = so

CXX           = g++
CXXFLAGS     += -O2 -c -DHTGEN
LD            = g++
LDFLAGS      +=
SOFLAGS       = -shared -fPIC
OutputOpt     = -o

OSTYPE = $(shell uname -s)
OSTYPE_contains_arwin = $(findstring arwin, $(OSTYPE) )

ifeq ($(findstring arwin, $(OSTYPE_contains_arwin)),arwin)
# MacOSX
  SOFLAGS = -dynamiclib -single_module -undefined dynamic_lookup
  DllSuf  = dylib
else
  CXXFLAGS += -fPIC
endif

###----------------------------------  objects
HTGENOB= $(OBJ)HTBaseProcess.o  $(OBJ)HTMscProcess.o  $(OBJ)HTMottProcess.o $(OBJ)HTBremProcess.o $(OBJ)HTConfiguration.o $(OBJ)HTTextParser.o $(OBJ)HTSixVector.o $(OBJ)HTErrorMessages.o $(OBJ)Mat3x3.o $(OBJ)Vec3.o

PLACETINTERFACEOB= $(OBJ)HTBaseInterface.o $(OBJ)HTPlacetInterface.o 

# MERLININTERFACEOB= $(OBJ)HTBaseInterface.o     \
#		   $(OBJ)HTMerlinInterface.o 

###---------------------------------- rules
all: $(OBJ) $(LIB) libhtgen libhtplacet

.PHONY: clean install

.NOTPARALLEL: install

clean :
	-rm -f $(OBJ)*.$(ObjSuf)  $(LIB)*.$(DllSuf)

$(OBJ):
	mkdir -p $(OBJ)

$(LIB):
	mkdir -p $(LIB)

$(OBJ)%.$(ObjSuf) : $(SRC)%.$(SrcSuf) $(INC)%.$(IncSuf)
	$(CXX) $(CXXFLAGS) $< -o $@  $(INCLPATH)

libhtgen: $(HTGENOB)
	@echo -e "\n \t [Makefile] Compile library $@"
	$(LD) $(LDFLAGS) $(SOFLAGS) $^ $(OutputOpt) $(LIB)$@.$(DllSuf)
	@echo -e "\n \t [Makefile] $@ done ..."

libhtplacet: $(PLACETINTERFACEOB)
	@echo -e "\n \t [Makefile] Compile library $@"
	$(LD) $(LDFLAGS) $(SOFLAGS) -L$(LIB) -lhtgen $^ $(OutputOpt) $(LIB)$@.$(DllSuf)

# libhtmerlin: $(MERLININTERFACEOB)
#	@echo -e "\n \t [Makefile] Compile library $@"
#	$(LD) $(SOFLAGS) $^ $(OutputOpt) $(LIB)$@.$(DllSuf)

# allow for make install
install: all

	mkdir -p $(DESTDIR)$(INSTALL_PREFIX)/lib
	mkdir -p $(DESTDIR)$(INSTALL_PREFIX)/include/htgen

	install lib/libhtgen.$(DllSuf)    $(DESTDIR)$(INSTALL_PREFIX)/lib/
	install lib/libhtplacet.$(DllSuf) $(DESTDIR)$(INSTALL_PREFIX)/lib/
	install -m 644 include/*.h $(DESTDIR)$(INSTALL_PREFIX)/include/htgen/

info:
	@echo "-------------------------------------"
	@echo  Info for htgen/Makefile
	@echo "-------------------------------------"
	@echo all is : libhtgen libhtplacet
	@echo LIB = $(LIB)
	@echo PLACET_DIR = $(PLACET_DIR)
	@echo ROOTSYS = $(ROOTSYS)
	@echo MERLINSRC = $(MERLINSRC)
	@echo SOFLAGS = $(SOFLAGS)
	@echo LDFLAGS = $(LDFLAGS)
	@echo CXXFLAGS = $(CXXFLAGS)
	@echo HTGENOB = $(HTGENOB)
	@echo PLACETINTERFACEOB = $(PLACETINTERFACEOB)
	@echo MERLININTERFACEOB = $(MERLININTERFACEOB)
	@echo INSTALL_PREFIX = $(INSTALL_PREFIX)
	@echo OSTYPE = $(OSTYPE)
	@echo OSTYPE_contains_arwin = $(OSTYPE_contains_arwin)
