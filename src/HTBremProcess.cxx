// htgen/src/HTBremProcess.cxx

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include "Vec3.h"
#include "Mat3x3.h"

#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTBremProcess.h"
#include "HTConstants.h"
#include "HTErrorMessages.h"

using namespace std;

HTBremProcess::HTBremProcess(string fname): HTBaseProcess(fname)
{
  process="brem";
  kmin = conf.kmin; // still starting from default kmin
  HT_DEBUG(" constructor with string to use configuration defined in file with fname=" << fname << " process=" << process);
}

HTBremProcess::HTBremProcess(HTConfiguration conf): HTBaseProcess(conf)
{
  process = "brem";
  kmin = conf.kmin;
  HT_DEBUG(" constructor with HTConfiguration for process=" << process << " thetamin=" << conf.thetamin);
}

double HTBremProcess::getCrossSection()
{
  double Z = conf.Zmean;
  HT_INFO("Z=" << Z << " kmin=" << kmin << " old sigma=" << sigma*u_Barn << " barn");
  sigma = (4.*c_alpha*c_re*c_re*Z*(Z+1.)*log(287./(sqrt(Z)))*(-4./3.*log(kmin) - 5./6. + 4./3.*kmin - kmin*kmin/2. )); // using the Dahl formula for the radiation length, http://pdg.lbl.gov/2008/reviews/passagerpp.pdf  Eq. (27.22)
  HT_INFO("Z=" << Z << " kmin=" << kmin << " new sigma=" << sigma*u_Barn << " barn");
  return sigma;
}

double HTBremProcess::getMeanFreePath(HTConfiguration par)
{
  double T = par.T;
  double pressure = par.pressure;
  double npart    = par.npart;
  kmin = par.kmin;
  sigma = getCrossSection();
  double NAtom=2;
  double lambda = c_k*T/pressure/sigma/npart/NAtom;
  HT_DEBUG("with config as parameter with T=" << T << " pressure=" << pressure << " kmin=" << kmin << " sigma=" << sigma << " lambda=" << lambda << " m");
  return lambda;
}

void HTBremProcess::kickParticle(double& xp,double& yp)
{
  bool moredebug=true;
  generateEvent(); // ---     do the Bremsstrahlung, generate xk, thetae, phie
  double thetae=asin(sinthetae);
  double phie= c_pi+phi;
  HT_DEBUG("HTBremProcess::kickParticle thetae=" << thetae << " phie=" << phie);
  Vec3 beam_in(xp,yp,sqrt(1. - xp*xp - yp*yp)); // incoming beam particle without before scatter
  Vec3 brem_dir( cos(phie)*sin(thetae), sin(phie)*sin(thetae), cos(thetae) ); // direction of mott scattering, if initial particle is in z direction
  Mat3x3 Rot=beam_in.GetRotToZMat();
  Vec3 beam_out=Transpose(Rot)*brem_dir; // rotate brem_dir to incoming beam
  if(getenv("HTGEN_DEBUG") && moredebug)
  {
    cout << "HTBremProcess with rotation" << '\n';
    cout << "            beam_in     beam_out       brem_dir" << '\n';
    cout << "X (xp)" << setw(13) << beam_in.r[0] << setw(13) << beam_out.r[0] << setw(13) << brem_dir.r[0] << '\n';
    cout << "Y (yp)" << setw(13) << beam_in.r[1] << setw(13) << beam_out.r[1] << setw(13) << brem_dir.r[1] << '\n';
    cout << "Z     " << setw(13) << beam_in.r[2] << setw(13) << beam_out.r[2] << setw(13) << brem_dir.r[2] << '\n';
  }
  HT_DEBUG("end of HTBremProcess::kickParticle");
  xp=beam_out.r[0];
  yp=beam_out.r[1];
}

void HTBremProcess::generateEvent()
{
  double ebeam = conf.ebeam;
  double gamma = ebeam/c_mele;
  int igen = 1;
  // generate the photon energy
  xk = pow(kmin,RanHtgen()); // generate of 1/x part
  while(RanHtgen() >  1.- xk + 3./4.*xk*xk) igen++; // rest with hit and miss
  thetap = sqrt(1./RanHtgen() - 1)/gamma;
  phi = 2.*c_pi*(RanHtgen()-0.5); // choose azimuth phi at random between -pi and +pi
  sinthetae =   xk/(1-xk)*sin(thetap);
  HT_DEBUG("igen=" << igen << " xk=" << xk << " thetap=" << thetap << " phi=" << phi << " asin(sinthetae)=" << asin(sinthetae) << " c_pi+phi=" << c_pi+phi);
}

void HTBremProcess::getGeneratedValues_Electron(double& e,double& theta,double& phi)
{
  e=1-xk;
  theta=asin(sinthetae);
  phi=c_pi+phi;
}

void HTBremProcess::getGeneratedValues_Photon(double& e,double& theta,double& phi)
{
  e=xk;
  theta=thetap;
  phi=this->phi;
}

int HTBremProcess::setKmin(double k)
{
  conf.kmin = k;
  return 0;
}


