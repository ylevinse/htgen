
# set lists of source files
set(htgen_files HTBaseProcess  HTMscProcess  HTMottProcess HTBremProcess HTConfiguration HTTextParser HTSixVector HTErrorMessages Mat3x3 Vec3)
set(placet_files HTBaseInterface HTPlacetInterface)

include_directories(${CMAKE_SOURCE_DIR}/include)

# HTGEN_DIR is typically not found during runtime,
# this will set rpath correctly in such situations:
if("$ENV{HTGEN_DIR}" STREQUAL "${CMAKE_INSTALL_PREFIX}")
   set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif()

# build htgen library
add_library(htgen SHARED ${htgen_files})
# On OSX, this makes the linking much easier..
if(APPLE)
    set_target_properties(htgen PROPERTIES INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")
endif()

install(TARGETS htgen
   LIBRARY DESTINATION lib
   COMPONENT Libraries)

if(EXISTS $ENV{PLACET_DIR}/include/placet.h)
    if(EXISTS $ENV{PLACET_DIR}/include/defaults.h)
        # We need to find the GSL headers in order to compile the htgen-placet interface:
        find_package(GSL REQUIRED)
        include_directories(${GSL_INCLUDE_DIRS})

        # Building htplacet..
        add_definitions(-DHTGEN)
        include_directories($ENV{PLACET_DIR}/include 
            $ENV{PLACET_DIR}/include-utils
            $ENV{PLACET_DIR}/src)

        add_library(htplacet SHARED ${placet_files})

        # On OSX we need this because the GSL functions are not available
        # Also, linking made easier as for libhtgen
        if(APPLE)
          set_target_properties(htplacet PROPERTIES LINK_FLAGS "-undefined dynamic_lookup"
            INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")
        endif()

        install(TARGETS htplacet
           LIBRARY DESTINATION lib
           COMPONENT Libraries)

        target_link_libraries(htplacet htgen)
    else()
        message(WARNING "You need to run ./configure for PLACET before building HTGEN")
    endif()
else()
    message(WARNING "You need to define environment variable PLACET_DIR")
endif()
