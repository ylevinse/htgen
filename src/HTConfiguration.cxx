// htgen/src/HTConfiguration.cxx

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
#include "HTErrorMessages.h"
#include "HTConfiguration.h"

HTConfiguration::HTConfiguration(): ebeam(1.),L(1.),T(300.),npart(1.),pressure(0),Zmean(0),thetamin(1.e-6),kmin(0.01){
  cout << "hbudebug in HTConfiguration pressure=" << pressure << '\n';
} // constructor with default values

int HTConfiguration::dump()
{
  HT_INFO(" ------------ Dump Configuration -------");
  cout << "ebeam    =" << ebeam << '\n';
  cout << "L        =" << L << '\n';
  cout << "T        =" << T << '\n';
  cout << "npart    =" << npart << '\n';
  cout << "press.   =" << pressure << '\n';
  cout << "Zmean    =" << Zmean << '\n';
  cout << "thetamin =" << thetamin << '\n';
  cout << "kmin     =" << kmin << '\n';
  return 1;
}
