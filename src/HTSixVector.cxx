// htgen/src/HTSixVector.cxx

#include <iostream> // for cin,cout,cerr,clog
#include <iomanip>  // i/o formatting like setprecision, setbase, flags
#include <string>
#include <sstream>
using namespace std;

#include "HTSixVector.h"

HTSixVector::HTSixVector(double x,double xp, double y, double yp, double ct, double dp)
{
  fill(x,xp,y,yp,ct,dp);
}
// ------------------------------------------------------------------------
double HTSixVector::x()
{
  return v[0];
}
// ------------------------------------------------------------------------
double HTSixVector::xp()
{
  return v[1];
}
// ------------------------------------------------------------------------
double HTSixVector::y()
{
  return v[2];
}
// ------------------------------------------------------------------------
double HTSixVector::yp()
{
  return v[3];
}
// ------------------------------------------------------------------------
double HTSixVector::ct()
{
  return v[4];
}
// ------------------------------------------------------------------------
double HTSixVector::dp()
{
  return v[5];
}
// ------------------------------------------------------------------------
int HTSixVector::set_x(double val)
{
  v[0] = val;
  return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::set_xp(double val)
{
  v[1] = val;
return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::set_y(double val)
{
  v[2] = val;
return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::set_yp(double val)
{
  v[3] = val;
return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::set_ct(double val)
{
  v[4] = val;
return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::set_dp(double val)
{
  v[5] = val;
return 0;
}
// ------------------------------------------------------------------------
int HTSixVector::fill(double x,double xp, double y, double yp, double ct, double dp)
{
  v[0] = x;
  v[1] = xp;
  v[2] = y;
  v[3] = yp;
  v[4] = ct;
  v[5] = dp;

  return 0;
}

string HTSixVector::PrintStr() //hbu added for debug. Print all six vector components to a string
{
  ostringstream StrOut;
  StrOut
    << " x="  << setw(13) << v[0]
    << " xp=" << setw(13) << v[1]
    << " y="  << setw(13) << v[2]
    << " yp=" << setw(13) << v[3]
    << " ct=" << setw(13) << v[4]
    << " dp=" << setw(13) << v[5];
    return StrOut.str();
}

