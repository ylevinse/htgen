/*
 subset of
  ~/c/MyClib/Vec3.C   written by H.Burkhardt
*/

#include <iostream>   // for cin,cout,cerr,clog
#include <iomanip>    // i/o formatting like setprecision, setbase, flags
#include <cmath>
// #include "Spherical.h" // to allow construct from Spherical in Vec3.h
#include "Vec3.h"
using namespace std;

Vec3 operator+ (const Vec3& vec1, const Vec3& vec2) //Stroustrup3 282
{ Vec3 res;
  // four vector addition, simply add all three components
  res.r[0]=vec1.r[0]+vec2.r[0]; // x
  res.r[1]=vec1.r[1]+vec2.r[1]; // y
  res.r[2]=vec1.r[2]+vec2.r[2]; // z
  return res;
}

Vec3 operator- (const Vec3& vec)    //Stroustrup3 282, change sign of vector part
{ Vec3 res;
  res.r[0]=-vec.r[0];
  res.r[1]=-vec.r[1];
  res.r[2]=-vec.r[2];
  return res;
}

Vec3 operator- (const Vec3& vec1,const Vec3& vec2){ // difference of 2 three vector
  Vec3 res;
  res.r[0]=vec1.r[0]-vec2.r[0];
  res.r[1]=vec1.r[1]-vec2.r[1];
  res.r[2]=vec1.r[2]-vec2.r[2];
  return res;
}

// define the cross product, for allowed operators see Stroustrup3 811
Vec3 operator^ (const Vec3& vec1, const Vec3& vec2) //Stroustrup3 282, vector cross product
{ Vec3 res;
  res.r[0]=vec1.r[1]*vec2.r[2]-vec1.r[2]*vec2.r[1];
  res.r[1]=vec1.r[2]*vec2.r[0]-vec1.r[0]*vec2.r[2];
  res.r[2]=vec1.r[0]*vec2.r[1]-vec1.r[1]*vec2.r[0];
  return res;
}

Vec3 operator* (double c, const Vec3& vec){ //Stroustrup3 282, factor * three vector
  Vec3 res;
  res.r[0]=c*vec.r[0];
  res.r[1]=c*vec.r[1];
  res.r[2]=c*vec.r[2];
  return res;
}

Vec3 operator/ (const Vec3& vec,double c) { // three vector divided by double
  Vec3 res;
  res.r[0]=vec.r[0]/c;
  res.r[1]=vec.r[1]/c;
  res.r[2]=vec.r[2]/c;
  return res;
}

void Vec3::ParallelPerp(const Vec3& b,Vec3& aParallel,Vec3& aPerp) {
// decompose r in vector rParallel and rPerp   parallel and perpendicular to b
  Vec3 a=*this;  // copy r in to Vec3 a
  aParallel=(a*b)*b /abs2(b);
  aPerp=a-aParallel;
}

const void Vec3::PrintObj(ostream& FilOut)
{ FilOut << " comp.(x,y,z)=(r[0],r[1],r[2])="
  << setw(13) << r[0] << " "
  << setw(13) << r[1] << " "
  << setw(13) << r[2] << '\n';
}

