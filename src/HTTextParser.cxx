// htgen/src/HTTextParser.cxx

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <cstdlib> // exit

#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTErrorMessages.h"

using namespace std;

static const double OneAtmos=101325.;           // One atmosphere in Pascal
static const double OneNanoTorr = 1.e-9*OneAtmos/760.;  // old pressure unit one Torr

// -----------------------------------------------------------------
HTTextParser::HTTextParser()
{
}

// -----------------------------------------------------------------
HTTextParser::HTTextParser(const char* name)
{
  parseFile(name);
}

// -----------------------------------------------------------------
int  HTTextParser::parseFile(const char* name)
{
  fname = name;
  fin.open(fname.c_str());
  isgood = fin.good();
  if(!isgood)
  {
    cout << "[HTTextParser::parseFile] Error " << fname << " not readeable ... exit!" << '\n';
    return 1;
  }
  init();
  return 0;
}

// -----------------------------------------------------------------
string HTTextParser::getFileName()
{
  return fname;
}

// -----------------------------------------------------------------
int HTTextParser::init()
{
  vector<double> par;
  int ipar = 0;
  while(!fin.eof())
  {
	string line;
	getline(fin,line); // read in one line from the input file
    istringstream iss(line);
    string word;
    if(line.size()>0) for( int i= 0; i<3; i++)
	// read a line like    Ebeam   : 1500.  : Beam Energy    with words separated by colon :
	// word                  0         1           2
	// only the number in word 1 is used
    {
      getline( iss, word, ':' );
      if(i==1) // only read the number in word on
      {
		double value;
        istringstream issd(word);
        issd >> value;
		if(issd) par.push_back(value); // add this value to the parameter vector
      }
    }
    ipar++;
  }
  HT_INFO(" par.size=" << par.size() );
  if(par.size()<8)
  {
	HT_INFO("Less then 8 input parameters par.size=" << par.size() );
	exit(1);
  }
  ebeam = par[0];
  Z = par[1];
  double PressureInNanoTorr = par[2];
  pressure = PressureInNanoTorr * OneNanoTorr; // pressure is in Pascal
  T = par[3];
  L = par[4];
  npart = par[5]*1e9;
  thetamin = par[6]*1e-6;
  kmin  = par[7];
  fillConfiguration();
  return 0;
}

// -----------------------------------------------------------------
int HTTextParser::print()
{
  cout << "-------------------------------------------" << '\n';
  cout << "PARAMETERS FOR FILE : " << fname << '\n';
  cout << "-------------------------------------------" << '\n';
  cout << "BEAM ENERGY [GeV] :" << ebeam << '\n';
  cout << "LENGTH      [m]   :" << L << '\n';
  cout << "Z mean            :" << Z << '\n';
  cout << "PRESSURE    [Pa]  :" << pressure << " = " << pressure/OneNanoTorr << " nTorr" << '\n';
  cout << "Temperature [K]   :" << T << '\n';
  cout << "NPart             :" << npart << '\n';
  cout << "THETAMIN    [rad] :" << thetamin << '\n';
  cout << "KMIN              :" << kmin << '\n';
  cout << "-------------------------------------------" << '\n';
  return 0;
}

// -----------------------------------------------------------------
HTConfiguration HTTextParser::getConfiguration()
{
  return conf;
}

// -----------------------------------------------------------------
int HTTextParser::fillConfiguration()
{
  conf.ebeam    = ebeam;
  conf.L        = L;
  conf.T        = T;
  conf.npart    = npart;
  conf.pressure = pressure;
  conf.Zmean    = Z;
  conf.thetamin = thetamin;
  conf.kmin = kmin;
  return 0;
}
