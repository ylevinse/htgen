// htgen/src/HTErrorMessages.cxx

#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>

#include "HTErrorMessages.h"

using namespace std;

int HTErrorMessages::printing_level = -1;

// ---------------------------------------------
HTErrorMessages::HTErrorMessages()
{
  if(printing_level == -1)
  {
    if( getenv( "HTGEN_DEBUG"))
    {
      strcpy(envvar,getenv("HTGEN_DEBUG") );
      printing_level= (int) strtol( envvar,0,0);
      cout << "HTErrorMessages::HTErrorMessages constructor printing_level=" << printing_level << '\n';
    }
  }
}

// ---------------------------------------------
HTErrorMessages::HTErrorMessages(int i)
{
  printing_level=i;
}

// ---------------------------------------------
int  HTErrorMessages::Message(int lvl, string arg, string msgtype, string filename, string functname, int linenr)
{
  if( printing_level >= lvl)
  {
    std::ostringstream lss;
	lss.flags(ios::floatfield | ios::right);
    lss << msgtype << filename <<  ":" << functname;
    if (linenr>=0) lss << ":" << linenr;
    lss << " " <<  arg; lss << std::endl;
    cout << lss.str();
  }
  return 0;
}

