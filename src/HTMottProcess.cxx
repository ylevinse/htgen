// htgen/src/HTMottProcess.cxx

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>

#include "Vec3.h"
#include "Mat3x3.h"

#include "HTConstants.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTErrorMessages.h"

using namespace std;

HTMottProcess::HTMottProcess(string fname): HTBaseProcess(fname)
{
  process="mott";
  HT_DEBUG(" constructor with string to use configuration defined in file with fname=" << fname << " process=" << process);
}

HTMottProcess::HTMottProcess(HTConfiguration conf): HTBaseProcess(conf)
{
  process  = "mott";
  HT_DEBUG(" constructor with HTConfiguration for process=" << process << " thetamin=" << conf.thetamin);
}

double HTMottProcess::getCrossSection()
{
  static const double C = 2.*c_pi* c_AlfaQEDHbarc*c_AlfaQEDHbarc;
  double thetamin = conf.thetamin; // minimum deflection angle
  // now calculate thetamin_factor = 1./(1.-cos(thetamin)) avoiding numerical cancellation
  // this value is kept in the class and also used in the generation of the distributions
  if(thetamin>1.e-6) thetamin_factor=1./(1.-cos(thetamin));
  else thetamin_factor=2./(thetamin*thetamin); // this avoids 1-cos rounding error cancellation
  double Z = conf.Zmean;
  double ebeam = conf.ebeam;
  sigma = C*Z*Z/(ebeam*ebeam)*thetamin_factor; // cross section sigma in m^2, stored in base class, see HTBaseProcess.h
  //conf.dump();
  HT_INFO("Z=" << Z << " ebeam=" << ebeam << " thetamin=" << thetamin << " sigma=" << sigma*u_Barn << " barn"); //provide same debug info level as in HTBremProcess.cxx
  return sigma;
}

double HTMottProcess::getMeanFreePath(HTConfiguration par) // get parameters from par
{
  double T        = par.T;
  double pressure = par.pressure;
  double npart    = par.npart;

  sigma = getCrossSection(); // calculate the cross section for this element
  double NAtom=2; // assumes we have molecules of two atoms
  double lambda = c_k*T/(pressure*sigma*npart*NAtom);
  HT_DEBUG("with config as parameter with T=" << T << " pressure=" << pressure << " sigma=" << sigma << " lambda=" << lambda << " m");
  return lambda;
}

void HTMottProcess::kickParticle(double& xp,double& yp)
{
   bool moredebug=true;
  // create an axis-vector for the rotation (incident particle)
  generateEvent();
  double costhetae=sqrt(1.-sinthetae*sinthetae);
  Vec3 beam_in(xp,yp,sqrt(1. - xp*xp - yp*yp)); // incoming beam particle without before scatter
  Vec3 mott_dir( cos(phi)*sinthetae, sin(phi)*sinthetae, costhetae ); // direction of mott scattering, if initial particle is in z direction
  Mat3x3 Rot=beam_in.GetRotToZMat();
  Vec3 beam_out=Transpose(Rot)*mott_dir; // rotate mott_dir to incoming beam
  if(getenv("HTGEN_DEBUG") && moredebug)
  {
    cout << "HTMottProcess with rotation" << '\n';
    cout << "directions  beam_in     beam_out       mott_dir" << '\n';
    cout << "X     " << setw(13) << beam_in.r[0] << setw(13) << beam_out.r[0] << setw(13) << mott_dir.r[0] << '\n';
    cout << "Y     " << setw(13) << beam_in.r[1] << setw(13) << beam_out.r[1] << setw(13) << mott_dir.r[1] << '\n';
    cout << "Z     " << setw(13) << beam_in.r[2] << setw(13) << beam_out.r[2] << setw(13) << mott_dir.r[2] << '\n';
  }
  xp=beam_out.r[0];
  yp=beam_out.r[1];
}

void HTMottProcess::generateEvent()
{
  int iacc = 0;
  int igen = 0;
  double beta = 1; // approximation
  while(!iacc)
  {
    igen++;
    // generate random -1 -> cos(thetamin)   --   careful with numerical cancellations --  still to be checked
    double z1 = RanHtgen()*(1./2.- thetamin_factor) + thetamin_factor;
    double zinv = 1./z1;
    double costhetae = 1.- zinv;
    //    if(zinv>1.8e-15) continue;
    double ratio = 1.- (beta*beta/2)*(1.-costhetae);
    if(RanHtgen()>ratio) continue;
    //    cout << "zinv = " << std::setw(14) << zinv << " et ratio = " << std::setw(14) << 1.-ratio << " et cosang = " << std::setw(14) << 1.-costhetae << endl;
    iacc++;
    sinthetae = sqrt(2.*zinv - zinv*zinv);
  }
  // generation of the angular distribution
  phi = 2.*c_pi*(RanHtgen()-0.5); // choose azimuth phi at random between -pi and +pi
  HT_DEBUG("sinthetae=" << sinthetae << " phi=" << phi << " x = " << cos(phi)*sinthetae*1.e6 << " y = " << sin(phi)*sinthetae << " rad");
}

void HTMottProcess::getGeneratedValues(double& e,double& theta,double& phi)
{
  e=1;
  theta=asin(sinthetae);
  phi=this->phi;
}

int HTMottProcess::setThetamin(double theta)
{
  conf.thetamin = theta;
  return 0;
}


