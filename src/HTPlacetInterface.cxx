// htgen/src/HTPlacetInterface.cxx

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "HTSixVector.h"

#include "HTConstants.h"
#include "HTErrorMessages.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"
#include "HTMscProcess.h"
#include "HTBaseInterface.h"
#include "HTPlacetInterface.h"

// placet headers
#include "structures_def.h" // also includes beam.h, with  get_slice_number_generic
#include "placeti3.h"
#include "select.h"

using namespace std;

string ParticleInfo(PARTICLE part, int ipart) // used to format HT_DEBUG printout
{
  ostringstream StrOut;
  StrOut << "Part ";
  if(ipart>0) StrOut << "#" << ipart;
  StrOut << " E = " << part.energy << " [x, xp, y, yp, z] = ["<< part.x << "," << part.xp << "," << part.y << "," << part.yp << "," << part.z << "]";
  return StrOut.str();
}

void HTPlacetInterface::print_htgen_info(BEAM* beam) //hbu use for debug to see what happens to beam
{
  if(beam==NULL)
  { cerr << "HTPlacetInterface::print_htgen_info beam==" << beam << "  I stop" << endl;
    exit(1);
  }
  else HT_DEBUG("beam nhalo=" << beam->nhalo << " n_max=" << beam->n_max << " slices_per_bunch=" << beam->slices_per_bunch << " macroparticles=" << beam->macroparticles);
}

HTPlacetInterface::HTPlacetInterface(): HTBaseInterface() // constructor, called from background.cc by HTGen constructor when trackbackground card is read
{
  interface = "placet";
  HT_DEBUG(" constructor interface=" << interface);
}

void HTPlacetInterface::makeEvent(BEAM* beam, ostringstream& scatt_str) // called from beamline_track_background via HTPlacetInterface::makeHalo
{
  // Choose a process
  string proctype = getProcessType();
  if(proctype=="notype")
  {
    HT_ERROR("Problem to choose a process");
    return;
  }
  int ipart = 0;
  PARTICLE pin = getRandomParticle(beam, ipart);
  //  pin.x = 0.; pin.xp = 0.; pin.y = 0.; pin.yp = 0.;
  //approximation: scattering process only changes position in the transverse plane => longitudinal position = const
  double xp = pin.xp * u_um; // conversion um->m [placet convention]
  double yp = pin.yp * u_um;
  HT_DEBUG(proctype << " BEFORE SCAT: xp=" << setw(13) << xp  << " yp=" << setw(13) << yp << " z=" << setw(13) << pin.z << " E=" << setw(13) << pin.energy);
  double theta=0;
  double e=0;
  double phi=0;
  if(proctype=="mott")
  {
    HT_DEBUG(" mott init thetamin=" << param.thetamin);
    mott->setThetamin(param.thetamin);
    mott->setEbeam(pin.energy);
    mott->kickParticle(xp,yp);
    mott->getGeneratedValues(e,theta,phi);
  }
  else if(proctype=="brem")
  {
    HT_DEBUG(" brem init kmin=" << param.kmin);
    brem->setKmin(param.kmin);
    brem->setEbeam(pin.energy);
    brem->kickParticle(xp,yp);
    brem->getGeneratedValues_Electron(e,theta,phi);
  }
  else
  {
    HT_WARNING("Is process " << proctype << " implemented? Nothing done!");
    return;
  }
  PARTICLE pout = pin;
  pout.xp = xp / u_um; // conversion m -> um [placet convention]
  pout.yp = yp / u_um;
  HT_DEBUG(proctype << " AFTER SCAT : xp=" << setw(13) << xp << " yp=" << setw(13) << yp  << " z=" << setw(13) << pout.z << " E=" << setw(13) << pout.energy);
  //save the scattering angle of each generated and scattered halo particle
  if(getenv("HTGEN_DEBUG"))  scatt_str << setw(13) << beam->nhalo << setw(14) << theta << setw(13) << phi << endl;
  // Now integrate this particle into the beam
  HT_DEBUG(" beam=" << beam << " before makeNewBeam ipart="<< ipart <<" beam->nhalo="<<beam->nhalo<<" beam->n_max="<<beam->n_max<<" DimHalo="<<DimHalo);
  makeNewBeam(beam, pout, ipart);
  return;
}

int HTPlacetInterface::makeMsc(PARTICLE &part, double length, double X0) // modifies part
{
  double energy = part.energy;
  if(energy <= 0)
  {
    HT_DEBUG(" *** warning *** initial " << ParticleInfo(part,0) << " energy should be positive. Nothing will be done with this particle");
    return 0;
  }
  HTSixVector vin(part.x, part.xp, part.y, part.yp,1,0);
  HTMscProcess msc;
  msc.setEb(energy); //hbu - maybe not actually needed
  msc.setX0(X0);
  msc.setPath(length);
  HTSixVector vout = msc.kickParticle(vin);
  HT_DEBUG(" initial " << ParticleInfo(part,0) << " vin" << vin.PrintStr() << " vout" << vout.PrintStr()); //hbu
  part.energy = energy*(1. - vout.dp());
  part.x  = vout.x();
  part.xp = vout.xp();
  part.y  = vout.y();
  part.yp = vout.yp();
  HT_DEBUG("   final " << ParticleInfo(part,0) << " energy loss by vout.dp()=" << vout.dp() << " length=" << length << " X0=" << X0 );
  return 0;
}

BEAM* HTPlacetInterface::makeDummyBeam(BEAM* beam) // only used in  htgen/src/TestPlacet.cxx   - consider to remove
{
  // default values
  int n_bunch = 1;
  int n_slice = 10;
  int n_macro = 10;
  int n_field=1;
  int n_max = 10000;
  beam = bunch_make(n_bunch, n_slice, n_macro, n_field, n_max,5000); // in placeti3.cc
  beam->bunches = n_bunch;
  beam->slices_per_bunch = n_slice;
  beam->macroparticles = n_macro;
  HT_DEBUG(" beam=" << beam << " beam->bunches="<<beam->bunches<<" beam->n_max="<<beam->n_max);

  double betax = 10.;
  double betay = 10.;
  double epsx = 1.e-6;
  double epsy = 1.e-6;
  double alphax = 1.;
  double alphay = 1.;
  double e0=250.;
  double delta=0.01;
  double ecut = 0.01;
  double de = 1.;
  double sigma_e = 0.;
  double zero_point = 0.;

  int k=0;
  for (int ibunch=0;ibunch<beam->bunches;ibunch++)
  {
    for (int islice=0;islice<beam->slices_per_bunch;islice++)
    {
      for (int ipart=0;ipart<beam->macroparticles;ipart++)
      {
        bunch_set_slice_x(beam,ibunch,islice,ipart,0.0);
        bunch_set_slice_xp(beam,ibunch,islice,ipart,0.0);
        bunch_set_sigma_xx(beam,ibunch,islice,ipart,betax,alphax,epsx);
        bunch_set_sigma_xy(beam,ibunch,islice,ipart);
        beam->particle[k].x=0.03*RanHtgen();
        beam->particle[k].xp=3000.*RanHtgen();
        bunch_set_slice_y(beam,ibunch,islice,ipart,zero_point);
        bunch_set_slice_yp(beam,ibunch,islice,ipart,0.0);
        bunch_set_sigma_yy(beam,ibunch,islice,ipart,betay,alphay,epsy);
        delta=-ecut+((double)ipart+0.5)*de;
        bunch_set_slice_energy(beam,ibunch,islice,ipart,e0+delta*sigma_e);
        beam->particle[k].y=0.004*RanHtgen();
        beam->particle[k].yp=20000*RanHtgen();
        beam->particle_number[k] = k;
        k++;
      }
    }
  }
  cout << "[HTPlacetInterface::makeBeam] nbunch = " << beam->bunches << '\n';
  return beam;
}

int HTPlacetInterface::dumpBeam(BEAM* beam)
{
  cout << "[HTPlacetInterface::dumpBeam] nbunch = " << beam->bunches << '\n';
  cout << "--------------------------------------------------------------" << '\n';
  cout << "[HTPlacetInterface::dumpBeam] ..........................." << '\n';
  int k = 0;
  for (int ibunch=0;ibunch<beam->bunches;ibunch++)
  {
    for (int islice=0;islice<beam->slices_per_bunch;islice++)
    {
      for (int ipart=0;ipart<beam->macroparticles;ipart++)
      {
        cout << "#" << k << " : part." << beam->particle_number[k]
        << " : [x, xp, y, yp] = ["
        << beam->particle[k].x << ","
        << beam->particle[k].xp << ","
        << beam->particle[k].y << ","
        << beam->particle[k].yp << "]"
        << '\n';
        k++;
      }
    }
  }
  cout << "--------------------------------------------------------------" << '\n';
  return 0;
}

PARTICLE HTPlacetInterface::getRandomParticle(BEAM* beam, int &ipart)
{
  PARTICLE part;
  // Use RandomSelect function of placet (select.h)
  int beam_slices  = beam->slices;
  RandomSelect *selector = new RandomSelect(beam_slices); // in select.h  use RandomSelect to select particles

  print_htgen_info(beam) ; // debug, useful in case of problems with beam

  for(int ip = 0; ip<beam_slices; ip++) // loop over all slices
  {
    selector->set_wgt(ip,beam->particle[ip].wgt); // copy the particle weight to selector weight
  }
  selector->init(); // sums up and normalizes weight distribution ?
  // ipart=selector->choose(rndm8_select()); // choose particle, code in select.h, make sure to init rndm8    rndm8_select is just a flat standard generator 0-1
  ipart=selector->choose(Select.Uniform()); // use a flat independent random generator from random.cc to pick out a particle

  delete selector;
  // in case of a sliced   beam: z-position of particle = z-position of corresponding slice (z-position of macroparticles is not set in a sliced beam)
  // in case of a particle beam: z-position of particle = z-position with respect to the bunch centre
  if(!beam->particle_beam)
  { // sliced beam
    one_particle(beam->sigma_xx+ipart, beam->sigma_xy+ipart, beam->sigma+ipart, beam->particle+ipart, &part);   //in select.cc
    int islice = beam->get_slice_number(ipart); // slice number of the macroparticle
    part.z=beam->z_position[islice];//z-position of particle is set to the z-position of the slice
    HT_DEBUG(" beam=" << beam << " ipart=" << ipart << " after ipart=selector->choose(rndm8_select) part. selected is " << setw(4) << ipart << " from " << beam_slices << " sliced beam case " << ParticleInfo(part,ipart));
    return part;
  }
  else
  { // particle beam
    HT_DEBUG(" beam=" << beam << " ipart=" << ipart << " after ipart=selector->choose(rndm8_select) part. selected is " << setw(4) << ipart << " from " << beam_slices << " particle beam case " << ParticleInfo(beam->particle[ipart],ipart));
    return beam->particle[ipart];
  }
}

int HTPlacetInterface::makeNewBeam(BEAM* beam, PARTICLE& part, int ipart)
{
  HT_DEBUG(" start beam=" << beam << " beam->n_max=" << beam->n_max << " ipart=" << ipart << " beam->nhalo=" << beam->nhalo << " DimHalo=" << DimHalo << " beam->nhalo % DimHalo=" << (beam->nhalo % DimHalo));
  beam->particle_sec[beam->nhalo]=part;
  beam->particle_sec[beam->nhalo].id=beam->nhalo; // give each halo particle an id for identification
  HT_DEBUG(" start beam=" << beam << " before beam->particle[ipart].id=beam->nhalo");
  beam->particle[ipart].id=beam->nhalo;
  beam->nhalo++;
  int islice = beam->get_slice_number_generic(ipart); // slice number of the macroparticle -> corresponds to z-position of the particle, which doesn't change during scattering, needed for the tracking in the cavities
  HT_DEBUG(" start beam=" << beam << " before beam->particle_number_sec[islice]++  islice=" << islice << " beam->particle_beam=" << beam->particle_beam);
  beam->particle_number_sec[islice]++; // update the number of halo particles per slice
  if (beam->nhalo % DimHalo == 0)
  {
    HT_INFO("beam->nhalo % DimHalo == 0. Before reallocating sec beam. Currently not supported - will exit");
    exit(1); // exit if ever getting here   and first make sure what about csrwake
    beam->particle_sec=(PARTICLE*)realloc(beam->particle_sec, sizeof(PARTICLE)*((beam->nhalo+DimHalo)/DimHalo)*DimHalo); //hbu  -- what about csrwake ??
  }
  HT_DEBUG("   end beam=" << beam << " beam->n_max=" << beam->n_max << " ipart=" << ipart << " beam->nhalo=" << beam->nhalo << " DimHalo=" << DimHalo << " beam->nhalo % DimHalo=" << (beam->nhalo % DimHalo));
  return 0;
}

int HTPlacetInterface::makeHalo(BEAM* beam, double length,ostringstream& scatt_str)
{
  // calculate the number of events
  int nevt=0;
  double lambda = getMeanFreePath(1); // recalculate for each element, 1 is a flag to trigger the recalculation of the meanfreepaths
  nevt = getNbEvents(lambda, length);
  if(nevt>0)
  {
    for(int i=0; i<nevt; i++) makeEvent(beam,scatt_str);
    HT_DEBUG(" beam=" << beam << " beam->nhalo=" << beam->nhalo << " beam->n_max=" << beam->n_max << " DimHalo=" << DimHalo << " lambda = " << lambda << " length = " << length << " gives nscat  nevt= " << nevt << " see also file motherid.tmp");
  }
  return nevt;
}

