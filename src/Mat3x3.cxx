/*
  written by H.Burkhardt before 2002
 from ~/c/MyClib/Mat3x3.C       previously (in 2002) in Conversion.C

*/

#include <iostream>   // for cin,cout,cerr,clog
#include <iomanip>    // i/o formatting like setprecision, setbase, flags
#include <cmath>
using namespace std;

//#include "Spherical.h"
#include "Vec3.h"
#include "Mat3x3.h"

Mat3x3 Vec3::GetRotToZMat(void) // returns the matrix which rotates the Vec3 to +z direction
{ // see fortran SUBROUTINE ROTTOZ H.Burkhardt 17-5-1993
  double SinPhi=r[1]/abs(*this);
  double CosPhi=sqrt(1.-SinPhi*SinPhi); // sign arbitrary ?
  double Theta=atan2(r[0],r[2]);    // atan2(y/x)   between 0 and 2pi, or -pi to +pi
  double cost=cos(Theta);
  double sint=sin(Theta);
  Mat3x3 Rot(cost,0,-sint,  -sint*SinPhi,CosPhi,-cost*SinPhi, sint*CosPhi,SinPhi,cost*CosPhi);
  return Rot;
}

const void Mat3x3::PrintObj(ostream& FilOut) // print the matrix
{ for(unsigned int irow=0; irow<3 ; irow++)
  { FilOut << "m[" << irow << ",0..2]=";
    for(unsigned int icol=0; icol<3 ; icol++) FilOut << setw(13) << m[irow][icol];
    FilOut << '\n';
  }
}

Mat3x3 Transpose(const Mat3x3& M)
{ Mat3x3 Mt;
  for(unsigned int irow=0; irow<3 ; irow++)
    for(unsigned int icol=0; icol<3 ; icol++) Mt.m[irow][icol]=M.m[icol][irow];
  return Mt;
}

Mat3x3 operator+ (const Mat3x3& mat1, const Mat3x3& mat2)   //Stroustrup3 282
{ Mat3x3 res;
  for(unsigned int irow=0; irow<3 ; irow++)
    for(unsigned int icol=0; icol<3 ; icol++) res.m[irow][icol]= mat1.m[irow][icol] + mat2.m[irow][icol];
  return res;
}

Mat3x3 operator- (const Mat3x3& mat1, const Mat3x3& mat2)   //Stroustrup3 282
{ Mat3x3 res;
  for(unsigned int irow=0; irow<3 ; irow++)
    for(unsigned int icol=0; icol<3 ; icol++) res.m[irow][icol]= mat1.m[irow][icol] - mat2.m[irow][icol];
  return res;
}

Mat3x3 operator* (const Mat3x3& m1,const Mat3x3& m2) // multiply two 3x3 matrices   cik = aij bjk
{ Mat3x3 M(0,0,0, 0,0,0, 0,0,0);
  const unsigned int n=3;
  for(unsigned int i=0; i<n ; i++)
    for(unsigned int j=0; j<n ; j++)
      for(unsigned int k=0; k<n ; k++)
        M.m[i][k]+=m1.m[i][j] * m2.m[j][k];
  return M;
}

Vec3 operator* (const Mat3x3& mat,const Vec3& v) // multiply a 3x3 matrix and a 3 vector
// see also fortran routine MATVEC
{ Vec3 res(0,0,0);
  for(unsigned int icol=0; icol<3 ; icol++)
    for(unsigned int irow=0; irow<3 ; irow++)
      res.r[irow]+=mat.m[irow][icol] * v.r[icol];
  return res;
}


