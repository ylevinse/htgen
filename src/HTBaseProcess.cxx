// htgen/src/HTBaseProcess.cxx

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"

using namespace std;



// supply a standard random generator to make this code library independent
// called here Ran0 to avoid clash with Ran1 in placet
double RanHtgen(void)
// Minimal Park-Miller generator with Bays-Durham shuffle
// result between 0 and 1 exclusive.
// should be of good quality but sequence limited to about 10**8 numbers
// similar to numerical recipes (in fortran page 271)
{ long const ia=16807;
  long const im=2147483647;
  long const iq=127773;
  long const ir=2836;
  long const ntab=32;
  long const ndiv=1+(im-1)/ntab;
  double const am=1./im;
  double const eps=1.2e-7;
  double const rnmx=1.-eps;
  
  static long IdumRan1=-123; // role of seed
  
  static long iv[ntab];
  static long iy=0;
  
  long k;
  if(IdumRan1<0||iy==0)
  {
	if(-IdumRan1>1) IdumRan1=-IdumRan1; else IdumRan1=1;
	for(long j=ntab+7; j>-1 ; j--)
	{
	  k=IdumRan1/iq;
	  IdumRan1=ia*(IdumRan1-k*iq)-ir*k;
	  if(IdumRan1<0) IdumRan1+=im;
	  if(j<ntab) iv[j]=IdumRan1;
	}
	iy=iv[0];
  }
  k=IdumRan1/iq;     // first action if not initializing
  IdumRan1=ia*(IdumRan1-k*iq)-ir*k; // compute mod(ia*IdumRan1,im) avoiding overflows
  if(IdumRan1<0) IdumRan1+=im;
  long j=iy/ndiv;
  iy=iv[j];
  iv[j]=IdumRan1;
  double result;
  if(am*iy<rnmx) result=am*iy; else result=rnmx;
  return result;
}


HTBaseProcess::HTBaseProcess(string n)
{
  parsername = n;
  init();
}

HTBaseProcess::HTBaseProcess(HTConfiguration c)
{
  parsername = "noparser";
  conf = c;
  init();
}

HTBaseProcess::HTBaseProcess()
{
  parsername = "noparser";
  init();
}

double HTBaseProcess::getCrossSection()
{
  return 0.;
}

int HTBaseProcess::generateDistribution(string type, int nevt)
{
  return 0;
}

HTTextParser* HTBaseProcess::getTextParser()
{
    return parser;
}

HTConfiguration HTBaseProcess::getConfiguration()
{
    return conf;
}

int HTBaseProcess::init()
{
  sigma = 0.;
  if(parsername != "noparser")
  {
    parser = new HTTextParser();
    if(parser->parseFile(parsername.c_str()))
    {
      cout << "[HTBaseProcess::init()] Problem with parsing file " << parsername << endl;
      return 1;
    }
    setConfiguration(parser);
    parser->print();
  }
  return 0;
}

int HTBaseProcess::setConfiguration(HTConfiguration c)
{
  conf = c;
  return 0;
}

int HTBaseProcess::setConfiguration(HTTextParser* parser)
{
  conf = parser->getConfiguration();
  return 0;
}

int HTBaseProcess::setEbeam(double energy)
{
  conf.ebeam = energy;
  return 0;
}

int HTBaseProcess::setTemperature(double T)
{
  conf.T = T;
  return 0;
}
