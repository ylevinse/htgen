#include "AcceleratorModel/StdComponent/Spoiler.h"
#include "MADInterface/MADInterface.h"
#include "NumericalUtils/PhysicalConstants.h"
#include "NumericalUtils/utils.h"
#include "Random/RandomNG.h"

#include "Constants.h"
#include "HTMerlinInterface.h"

using namespace ParticleTracking;
using namespace PhysicalConstants;
using namespace PhysicalUnits;

extern void ScatterParticle(PSvector&, double t, double l, double E0);

void OutputIndexParticles(const PSvectorArray lost_p, const list<size_t>& lost_i, ostream& os)
{
    PSvectorArray::const_iterator p = lost_p.begin();
    list<size_t>::const_iterator ip = lost_i.begin();

    while(p!=lost_p.end()) {
        os<<std::setw(12)<<right<<*ip;
        os<<*p;
        ++p;
        ++ip;
    }
}
// ----------------------------------------------------------------------------
// HTMerlinInterface
// ----------------------------------------------------------------------------
HTMerlinInterface::HTMerlinInterface() : HTBaseInterface()
{
  init();
}
// ---------------------------------------------------------
HTMerlinInterface::HTMerlinInterface(const string& lattice) : HTBaseInterface()
{
  pair<AcceleratorModel*, BeamData*> themodel = constructModel(lattice);
  theacc  = themodel.first;
  thebeam = themodel.second;
  init();
}
// ---------------------------------------------------------
HTMerlinInterface::~HTMerlinInterface()
{
}
// ---------------------------------------------------------
int HTMerlinInterface::init()
{
  RandomNG::init();
  return 1;
}
// ---------------------------------------------------------
pair<AcceleratorModel*, BeamData*> HTMerlinInterface::constructModel(const string& fname)
{
  // Here we
  // 1. construct the initial (matched) beam data
  // 2. construct the AcceleratorModel from the supplied
  //    file.

  double P0 = 1500.0*GeV;
  double gamma = P0/(ElectronMassMeV*MeV);

  BeamData *beam = new BeamData;
  //  beam->beta_x = 7.e-3;
  beam->beta_x = 64.;
  beam->sig_z  = 0.09e-3;
  //  beam->beta_y = 0.15e-3;
  beam->beta_y = 18.;
  beam->sig_dp = 1.5e-03;
  beam->emit_x = 1.0e-06/gamma;
  beam->emit_y = 0.01e-06/gamma;
  beam->charge = .4e+10;
  beam->p0=P0;

  MADInterface mad(fname,P0);

  ofstream madlog("mad.log");
  mad.SetLogFile(madlog);
  mad.SetLoggingOn();

  mad.ConstructApertures(true);
  mad.ConstructFlatLattice(true);

  // MADInterface knows nothing about the following types,
  // so we treat them as drifts.
  mad.TreatTypeAsDrift("ABSORBER");
  mad.TreatTypeAsDrift("SAMPLER");
  mad.TreatTypeAsDrift("SPOILER");
  mad.TreatTypeAsDrift("MULTIPOLE");
  mad.TreatTypeAsDrift("MARKER");

  AcceleratorModel* model = mad.ConstructModel();
  return make_pair(model,beam);
}
ParticleBunch* HTMerlinInterface::constructBunch(int npart)
{
//Checkout beamdata from mad
  cout<<"thebeam->sigma_x  = "<<thebeam->sigma_x()<<endl;
  cout<<"thebeam->sigma_y  = "<<thebeam->sigma_y()<<endl;
  cout<<"thebeam->sigma_xp = "<<thebeam->sigma_xp()<<endl;
  cout<<"thebeam->sigma_yp = "<<thebeam->sigma_yp()<<endl;
  thebunch = new ParticleBunch(thebeam->p0);

  Particle p(0);
  double gamma_x = (1+pow(thebeam->alpha_x,2))/thebeam->beta_x;
  double gamma_y = (1+pow(thebeam->alpha_y,2))/thebeam->beta_y;

  double sigx = sqrt(thebeam->emit_x*thebeam->beta_x);
  double sigxp = sqrt(thebeam->emit_x*gamma_x);
  double sigy = sqrt(thebeam->emit_y*thebeam->beta_y);
  double sigyp = sqrt(thebeam->emit_y*gamma_y);

  double lx  = 1*sigx;
  double lxp = 1*sigxp;
  double ly  = 1*sigy;
  double lyp = 1*sigyp;
  double ldp = 0.001;
  cout <<"sigxp = "<<sigxp<<" sigyp = "<<sigyp<<endl;
  for(int ipart = 0; ipart<1000000; ipart++) {
    p.x()  = RandomNG::uniform(-lx,lx)+thebeam->x0;
    p.xp() = RandomNG::uniform(-lxp,lxp)+thebeam->xp0;
    p.y()  = RandomNG::uniform(-ly,ly)+thebeam->y0;
    p.yp() = RandomNG::uniform(-lyp,lyp)+thebeam->yp0;
    p.dp() = RandomNG::uniform(-ldp,ldp);
    p.ct() = 0;
    thebunch->AddParticle(p);
  }
  return thebunch;
}
AcceleratorModel* HTMerlinInterface::getAcceleratorModel()
{
  return theacc;
}
int HTMerlinInterface::makeHalo(double length)
{
  int nevt=0;
  double lambda = getMeanFreePath(1); // recalculate for each element
  nevt = getNbEvents(lambda, length);
  HT_DEBUG("lambda = "<<lambda<<" length = "<<length<<" give nscat = "<<nevt);
  if(nevt==0) return 0;
  // boucle
  for(int i=0; i<nevt; i++) {
    makeEvent();
  }
  return nevt;
}
int HTMerlinInterface::makeEvent()
{
  string proctype = getProcessType();
  if(proctype=="notype") {
    HT_ERROR("Problem to choose a process");
    return 1;
  }
  int ipart = 0;
  Particle p = getRandomParticle(ipart);

  pair<double, double> partin, partout;
  partin = make_pair(p.xp()*c_c, p.yp()*c_c);

  if(proctype=="mott") {
    mott->setThetamin(param.thetamin);
    //    mott->setEbeam(p.energy);
    partout = mott->kickParticle(partin);
  }
  else if(proctype=="brem") {
    brem->setKmin(param.kmin);
    //    brem->setEbeam(p.energy);
    partout = brem->kickParticle(partin);
  }
  else {
    HT_WARNING("Is process "<<proctype<<" implemented? Nothing done!");
    return 1;

  }
  p.xp() = partout.first/c_c;
  p.yp() = partout.second/c_c;

  HT_DEBUG("BEFORE SCAT: xp="<<partin.first<<" yp="<<partin.second);
  // dumpParticle(pin, ipart);
  HT_DEBUG("AFTER SCAT : xp="<<partout.first<<" yp="<<partout.second);

  thebunch->AddParticle(p);

  return 0;
}
PSvector HTMerlinInterface::getRandomParticle(int &ipart)
{
  ipart = (int) (rand()/(double)RAND_MAX * thebunch->size());
  return thebunch->GetParticles()[ipart];
}
// ----------------------------------------------------------------------------
// HTGenParticleProcess
// ----------------------------------------------------------------------------
HTGenParticleProcess::HTGenParticleProcess(int prio, int mode, std::ostream* osp):
  ParticleBunchProcess("Halo Generation",prio), cmode(mode),os(osp),
  createLossFiles(false),file_prefix(""),lossThreshold(1), nstart(0),
  pindex(0),scatter(false)
{
}
// ---------------------------------------------------------
HTGenParticleProcess::~HTGenParticleProcess()
{
}
// ---------------------------------------------------------
int HTGenParticleProcess::setHTInterface(HTMerlinInterface* interface)
{
  theiface = interface;
  return 0;
}
// ---------------------------------------------------------
void HTGenParticleProcess::InitialiseProcess (Bunch& bunch)
{
  ParticleBunchProcess::InitialiseProcess(bunch);
  idtbl.clear();
  if(currentBunch) {
    nstart = currentBunch->size();
    nstart0 = nstart;
    nlost = 0;
    nhalo = 0;
    pindex = new list<size_t>;
    if(pindex!=0) {
      pindex->clear();
      for(size_t n=0; n<nstart; n++)
    pindex->push_back(n);
    }
  }
  idcomp = 0;
}
// ---------------------------------------------------------
void HTGenParticleProcess::SetCurrentComponent (AcceleratorComponent& component)
{
  active = (currentBunch!=0) && (component.GetAperture()!=0);

  if(active) {
    currentComponent = &component;
    s=0;
    Spoiler* aSpoiler = dynamic_cast<Spoiler*>(&component);
    is_spoiler = scatter && aSpoiler;

    if(!is_spoiler) { // not a spoiler so set up for normal hard-edge collimation
      at_entr = (COLL_AT_ENTRANCE & cmode)!=0;
      at_cent = (COLL_AT_CENTER & cmode)!=0;
      at_exit = (COLL_AT_EXIT & cmode)!=0;
      SetNextS();
    }
    else {
      at_entr=at_cent=false;
      at_exit = true;
      SetNextS();
      Xr = aSpoiler->GetNumRadLengths();
      len = aSpoiler->GetLength();
    }
  }
  else {
    s_total += component.GetLength();
    currentComponent = 0;
  }
}
// ---------------------------------------------------------
void HTGenParticleProcess::DoProcess(double ds)
{
  s+=ds;
  if(fequal(s,next_s)) {
    DoHalo();
    DoCollimation();
    cout<<"[HTGenParticleProcess::DoProcess] size = "<<currentBunch->size()
    <<" nhalo = "<<nhalo<<" frac lost = "<<(double) nlost/nhalo<<" pour "<<s_total<<" m"<<endl;
    SetNextS();
  }

  // If we are finished, GetNextS() will have set the process inactive.
  // In that case we can update s_total with the component length.
  if(!active)
    s_total += currentComponent->GetLength();
}
// ---------------------------------------------------------
int HTGenParticleProcess::DoHalo()
{
  double complength = currentComponent->GetLength();
  HT_INFO("Component :"<<currentComponent->GetName());
  HT_DEBUG("nb = "<<idcomp<<" length ="<<complength);

  int nscat = 0;
  if(complength==0.) return 1;
  nscat = theiface->makeHalo(complength);
  nhalo +=nscat;
  if(pindex!=0) {
    for(int i=(int) nhalo; i<(int) nhalo+nscat; i++)
      pindex->push_back((size_t) 10*nstart0 + i);
  }

  //for(list<size_t>::iterator i=pindex->begin(); i != pindex->end(); i++)
  //cout<<"nhalo = "<<nhalo<<" pindex = "<<(*i)<<endl;
  // recalculate bunch size
  nstart = currentBunch->size();

  // FIN MOTT SCATTERING

  string fname = "out_scat.txt";
  ofstream fout(fname.c_str(), ofstream::out | ofstream::app);
  fout <<s_total<<"  "<<nscat<<"   "<<currentComponent->GetName()<<endl;
  fout.close();
  return 0;
}
// ---------------------------------------------------------
int HTGenParticleProcess::DoCollimation()
{
  idcomp++;
  const Aperture *ap = currentComponent->GetAperture();

  PSvectorArray lost;
  list<size_t>  lost_i;

  list<size_t>::iterator ip;
  if(pindex!=0)
    ip=pindex->begin();

  for(PSvectorArray::iterator p = currentBunch->begin(); p!=currentBunch->end(); p++) {
    if(!ap->PointInside((*p).x(),(*p).y(),s)) {
      cout<<"partile lost"<<endl;
      if(!is_spoiler || DoScatter(*p)) {
    lost.push_back(*p);
    p=currentBunch->erase(p);
    --p;
    if(pindex!=0) {
      lost_i.push_back(*ip);
      ip = pindex->erase(ip);
      --ip;
    }
      }
    }
    if(pindex!=0)
      ++ip;
  }
  nlost+=lost.size();
  DoOutput(lost,lost_i);

  ofstream out2;
  out2.open ("out_loss.txt", ofstream::out | ofstream::app);
  out2 <<s_total<<"  "<<lost.size()<<"   "<<currentComponent->GetName()<<endl;;
  out2.close();

  //  if(double(nlost)/double(nstart)>=lossThreshold)
  //throw ExcessiveParticleLoss(currentComponent->GetQualifiedName(),lossThreshold,nlost,nstart);

  return 0;
}
// ---------------------------------------------------------
int HTGenParticleProcess::SetNextS()
{
    if(at_entr) {
        next_s=0;
        at_entr=false;
    }
    else if(at_cent) {
        next_s=currentComponent->GetLength()/2;
        at_cent=false;
    }
    else if(at_exit) {
        next_s=currentComponent->GetLength();
        at_exit = false;
    }
    else
        active=false;

  return 0;
}
// ---------------------------------------------------------
void HTGenParticleProcess::CreateParticleLossFiles (bool flg, string fprefix)
{
  createLossFiles = flg;
  file_prefix = fprefix;
}
// ---------------------------------------------------------
void HTGenParticleProcess::SetLossThreshold (double losspc)
{
    lossThreshold = losspc/100.0;
}
// ---------------------------------------------------------
void HTGenParticleProcess::IndexParticles (bool index)
{
//   if(index && pindex==0)
//     pindex = new list<size_t>;
//   else if(!index && pindex!=0) {
//     delete pindex;
//     pindex=0;
//   }
}
// ---------------------------------------------------------
void HTGenParticleProcess::IndexParticles (list<size_t>& anIndex)
{
//     if(!pindex)
//         delete pindex;

    pindex=&anIndex;
}
// ---------------------------------------------------------
double HTGenParticleProcess::GetMaxAllowedStepSize () const
{
  return next_s-s;
}
bool HTGenParticleProcess::DoScatter (Particle& p)
{
    double E0=currentBunch->GetReferenceMomentum();
    ScatterParticle(p,Xr,len,E0);
    return p.dp()<=-0.99; // return true if E below 1% cut-off
}
void HTGenParticleProcess::DoOutput (const PSvectorArray& lostb, const list<size_t>& lost_i)
{
    // Create a file and dump the lost particles
    // (if there are any)
    if(!lostb.empty()) {
        if(os!=0) {
            (*os)<<std::setw(24)<<left<<(*currentComponent).GetQualifiedName().c_str();
            (*os)<<std::setw(12)<<right<<currentComponent->GetLength();
            (*os)<<std::setw(12)<<right<<currentBunch->GetReferenceTime();
            (*os)<<std::setw(8)<<right<<lostb.size()<<endl;
        }
        if(createLossFiles) {
            string id = (*currentComponent).GetQualifiedName();
            pair<IDTBL::iterator,bool> result = idtbl.insert(IDTBL::value_type(id,0));
            int n = ++(*(result.first)).second;
            ostringstream fname;
            if(!file_prefix.empty())
          fname<<file_prefix;
            fname<<id<<'.'<<n<<".loss";
            ofstream file(fname.str().c_str());
            if(pindex==0)
          copy(lostb.begin(),lostb.end(),ostream_iterator<PSvector>(file));
            else
          OutputIndexParticles(lostb,lost_i,file);
        }
    }
}


