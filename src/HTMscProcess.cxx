// htgen/src/HTMscProcess.cxx

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>

#include "HTSixVector.h"

#include "HTConstants.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTErrorMessages.h"
#include "HTMscProcess.h"

using namespace std;

void RanNorm(double *result1,double *result2)
//hbu normally distributed random pair, with zero mean and unit variance
{
  double v1,v2,RadiusSquared,fac;
  do
  {
    v1=2.*RanHtgen()-1.; // two random between
    v2=2.*RanHtgen()-1.; // -1 and 1
    RadiusSquared=v1*v1+v2*v2;
  }
  while(RadiusSquared>1. || RadiusSquared==0.);
  fac=sqrt(-2.*log(RadiusSquared)/RadiusSquared);
  *result1 =v1*fac;
  *result2 =v2*fac;
}

HTSixVector HTMscProcess::kickParticle(HTSixVector vin)
{
  // getLoss(0); // initialize the loss distribution   done each time ?
  HTSixVector vout;
  double dploss = getLoss(); // get randomly the ionization loss
  vout.set_dp(vin.dp() + dploss);
  double cx1,cx2;
  ScatWithGauss(cx1,cx2);
  double cy1,cy2;
  ScatWithGauss(cy1,cy2);
  vout.fill(vin.x() + cx2, vin.xp() + cx1,
            vin.y() + cy2, vin.yp() + cy1,
            vin.ct(), vout.dp()); // hbu had vin.dp() so that dp never changed
  HT_DEBUG(" kickParticle ebeam=" << ebeam << " vin" << vin.PrintStr() << " dploss=" << dploss << " vout" << vout.PrintStr()); //hbu
  return vout;
}

void HTMscProcess::ScatWithGauss(double& g1,double& g2)
{
  double beta = 1; // approximation
  double p = ebeam * 1.e3; // GeV -> MeV
  double theta0=13.6*sqrt(length/X0)/(p*beta)*(1.+0.038*log(length/X0)); // see PDF multiple scattering, theta0 is rms of projected angular distribution
  double z1,z2;
  RanNorm(&z1,&z2); // get two normal distributed numbers
  z1 = theta0*z1;
  z2 = theta0*z2;
  g1 = z1;
  g2 = z1*length/2. + z2*length/sqrt(12.);
  HT_DEBUG("end of HTMscProcess::ScatWithGauss z1=" << z1 << " z2=" << z2 << " will make a pair, 1st is z1 and 2nd=" << g2 << " theta0=" << theta0 );
}

double HTMscProcess::getLoss()
{
  return 0; // temporarily no energy loss in multiple scattering. Will have to be rewritten.
}
