// htgen/src/HTBaseInterface.cxx

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"

#include "HTBaseInterface.h"
#include "HTErrorMessages.h"

using namespace std;

HTBaseInterface::HTBaseInterface() // constructor, called as base class from HTPlacetInterface constructor which is called in background.cc by HTGen constructor when trackbackground card is read
{
  HT_DEBUG(" constructor");
}

int HTBaseInterface::addProcess(string name) // typically called from background.cc via tk_TrackBackground, beamline_track_background. Add proccess and init mean free path
{
  HT_DEBUG("HTBaseInterface::addProcess string name =" << name);
  if(name=="mott")
  {
    mott = new HTMottProcess(param); // using HTConfiguration param
    mprocess[name] = 0; // mott->getMeanFreePath(param); // meanfreepath using HTConfiguration default values
  }
  else if(name=="brem")
  {
    brem = new HTBremProcess(param); // using HTConfiguration param
    mprocess[name] = 0; // brem->getMeanFreePath(param); // meanfreepath using HTConfiguration default values
  }
  else
  {
    HT_ERROR("Process " << name << " does not exist! ");
    return 1;
  }
  return 0;
}

int HTBaseInterface::init()
{
  return 0;
}

double HTBaseInterface::getMeanFreePath(int flg)
{
  // 1/L = 1/L1 + 1/L2 + ...
  double sum = 0;
  if (flg==1)
  { // recalculate meanfreepaths
    for(ProcessMap::iterator itr = mprocess.begin(); itr != mprocess.end(); itr++)
    {
      if((*itr).first=="mott")
      {
        mott->setConfiguration(param);
        (*itr).second = mott->getMeanFreePath(param); // meanfreepath
      }
      if((*itr).first=="brem")
      {
        brem->setConfiguration(param);
        (*itr).second = brem->getMeanFreePath(param); // meanfreepath
      }
      sum += 1./(*itr).second;
    }
  }
  else
  {
    for(ProcessMap::iterator itr = mprocess.begin(); itr != mprocess.end(); itr++)
    {
      sum += 1./(*itr).second;
    }
  }
  double res = 1./sum;
  HT_DEBUG(" flg=" << flg << " Mean Free Path is res=1/sum=" << res << " m");
  return res;
}

string HTBaseInterface::getProcessType()
{
  double cumulwgth = 0.;
  double lambda = getMeanFreePath();
  for(ProcessMap::iterator itr = mprocess.begin(); itr != mprocess.end(); itr++)
  {
    cumulwgth += lambda/(*itr).second;
    if(RanHtgen()<cumulwgth)
    {
      HT_DEBUG("Process will be " << (*itr).first << " (cumul=" << cumulwgth << " )");
      return (*itr).first;
    }
  }
  return "notype";
}

int HTBaseInterface::allProcess()
{
  addProcess("mott");
  addProcess("brem");
  return 0;
}

int HTBaseInterface::setConfiguration(HTConfiguration c)
{
  param = c;
  return 0;
}

int HTBaseInterface::setPressure(double p)
{
  HT_DEBUG("pressure=" << p);
  param.pressure = p;
  return 0;
}

int HTBaseInterface::setNpart(double npart)
{
  param.npart = npart;
  return 0;
}

int HTBaseInterface::setTemperature(double t)
{
  param.T = t;
  return 0;
}

int HTBaseInterface::setZ(double z)
{
  param.Zmean = z;
  return 0;
}

int HTBaseInterface::setThetamin(double thetamin)
{
  param.thetamin = thetamin;
  return 0;
}

int HTBaseInterface::setKmin(double kmin)
{
  param.kmin = kmin;
  return 0;
}

int HTBaseInterface::setEbeam(double ebeam)
{
  param.ebeam = ebeam;
  return 0;
}

HTConfiguration HTBaseInterface::getConfiguration()
{
  return param;
}

int HTBaseInterface::getNbEvents(double lambda, double length, int fstep)
{
  int nscat = 0;
  double  ratio = length/lambda;
  int nstep = (int) (fstep + fstep*ratio);
  for(int istep=0; istep< nstep; istep++)
  {
    if(RanHtgen() < (1. - exp(- ratio/(double) nstep))) nscat++;
  }
  return nscat;
}

